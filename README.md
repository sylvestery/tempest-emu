# Tempest Emulator

## How To Run

INES files can be loaded on application start. 

Currently the Emulator only supports roms using mapper 0. 

## NES Emulation Status

- [ ] CPU
  - [x] Official Op Codes
  - [ ] Illegal OP Codes 
  - [ ] Debug Mode
  - [ ] Interrupts
    - [x] NMI
    - [ ] IRQ
- [ ] Clocks
  - [x] Tick on BUS Update
  - [x] Tick on cpu cycle
  - [x] Tick on PPU cycle
- [ ] PPU
  - [ ] NameTable Mirroring
    - [x] Horizontal
    - [x] Vertical
    - [x] Diagonal
    - [x] FourScreen
    - [ ] Single-Screen
    - [ ] 3 Screen
  - [ ] Backgrounds
  - [ ] Foreground
  - [ ] CHRAM
- [ ] APU
  - [ ] Pulse
  - [ ] Triangle
  - [ ] Noise
- [ ] Render
- [ ] BUS
- [ ] Joypad
- [ ] Carts
  - [ ] Mappers
    - [x] NROM
    - [ ] SxRom
    - [ ] UxRom
    - [ ] CNROM
- [ ] Testing

 

## References

- [NESDOC](https://www.nesdev.com/NESDoc.pdf)

- [6502 Reference](http://www.obelisk.me.uk/6502)
- [Nesdev 6502 Reference](http://nesdev.com/6502.txt)
- [Tempest Memory Map](http://www.kfu.com/~nsayer/games/tempest.html)
- [Vector State Machine](http://www.kfu.com/~nsayer/games/vecops.txt)