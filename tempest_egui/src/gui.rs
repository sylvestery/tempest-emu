use crate::Emu;
use egui::emath;
use egui::{containers::*, *};
use egui::{ClippedMesh, FontDefinitions, Slider};
use egui_wgpu_backend::{RenderPass, ScreenDescriptor};
use egui_winit_platform::{Platform, PlatformDescriptor};
use pixels::{wgpu, PixelsContext};
use std::collections::HashMap;
use std::time::Instant;
use tempest_core::map::{Addressable, BusDevice};
use tempest_core::mos6502::cpu::{Register, Status, MOS6502};
use tempest_core::ppu::palette::DEFAULT_PALETTE;
use tempest_core::NES;
/// Manages all state required for rendering egui over `Pixels`.
pub(crate) struct Gui {
    // State for egui.
    start_time: Instant,
    platform: Platform,
    screen_descriptor: ScreenDescriptor,
    rpass: RenderPass,
    paint_jobs: Vec<ClippedMesh>,
    palette: u8,

    // State for the demo app.
    menu_options: HashMap<String, bool>,
}

impl Gui {
    /// Create egui.
    pub(crate) fn new(width: u32, height: u32, scale_factor: f64, pixels: &pixels::Pixels) -> Self {
        let platform = Platform::new(PlatformDescriptor {
            physical_width: width,
            physical_height: height,
            scale_factor,
            font_definitions: FontDefinitions::default(),
            style: Default::default(),
        });
        let screen_descriptor = ScreenDescriptor {
            physical_width: width,
            physical_height: height,
            scale_factor: scale_factor as f32,
        };
        let rpass = RenderPass::new(pixels.device(), pixels.render_texture_format(), 1);

        Self {
            start_time: Instant::now(),
            platform,
            screen_descriptor,
            rpass,
            paint_jobs: Vec::new(),
            menu_options: HashMap::new(),
            palette: 0,
        }
    }

    /// Handle input events from the window manager.
    pub(crate) fn handle_event(&mut self, event: &winit::event::Event<'_, ()>) {
        self.platform.handle_event(event);
    }

    /// Resize egui.
    pub(crate) fn resize(&mut self, width: u32, height: u32) {
        if width > 0 && height > 0 {
            self.screen_descriptor.physical_width = width;
            self.screen_descriptor.physical_height = height;
        }
    }

    /// Update scaling factor.
    pub(crate) fn scale_factor(&mut self, scale_factor: f64) {
        self.screen_descriptor.scale_factor = scale_factor as f32;
    }

    /// Prepare egui.
    pub(crate) fn prepare(&mut self, emu: &mut Emu) {
        self.platform
            .update_time(self.start_time.elapsed().as_secs_f64());

        // Begin the egui frame.
        self.platform.begin_frame();

        // Draw the application.
        self.ui(&self.platform.context(), emu);

        // End the egui frame and create all paint jobs to prepare for rendering.
        let (_output, paint_commands) = self.platform.end_frame();
        self.paint_jobs = self.platform.context().tessellate(paint_commands);
    }

    fn render_instruction_trace(&mut self, cpu: &mut MOS6502, ctx: &egui::CtxRef) {
        let instruction_trace = cpu.disassembler(cpu.pc() - 20..cpu.pc() + 20);
        egui::Window::new("Instruction Trace").show(ctx, |ui| {
            for (addr, line) in instruction_trace.iter() {
                if &cpu.pc() == addr {
                    ui.colored_label(Color32::GREEN, line);
                } else {
                    ui.label(line);
                }
            }
        });
    }

    fn render_status(&mut self, ctx: &egui::CtxRef, cpu: &MOS6502) {
        egui::Window::new("Status")
            .fixed_size((300.0, 150.0))
            .open(self.get_menu_option("option_status"))
            .show(ctx, |ui| {
                ui.label("Registers");
                ui.label(format!("A: {:#04X}", cpu.register(Register::A)));
                ui.label(format!("X: {:#04X}", cpu.register(Register::X)));
                ui.label(format!("Y: {:#04X}", cpu.register(Register::Y)));
                ui.separator();
                ui.label(format!("PC: {:#06X}", cpu.pc()));
                ui.label(format!("SP: {:#04X}", cpu.sp()));
                ui.separator();
                let mut colored_text = |status, color| {
                    if cpu.get_flag(status) {
                        ui.colored_label(color, format!("{:?}", status));
                    } else {
                        ui.label(format!("{:?}", status));
                    }
                };
                colored_text(Status::InterruptDisable, Color32::RED);
                colored_text(Status::Break, Color32::RED);

                colored_text(Status::Negative, Color32::BLUE);
                colored_text(Status::Zero, Color32::GREEN);
                colored_text(Status::Overflow, Color32::YELLOW);
                colored_text(Status::Carry, Color32::LIGHT_BLUE);
                colored_text(Status::Decimal, Color32::RED);
            });
    }

    fn render_pattern_table(&mut self, emu: &mut Emu, ctx: &egui::CtxRef) {
        let cart = emu.cart.borrow();
        let ppu = emu.nes.ppu.borrow();
        egui::Window::new("Patterns")
            .fixed_size((300.0, 150.0))
            .show(ctx, |ui| {
                ui.horizontal(|ui| {
                    ui.label("Palette: ");
                    for i in 0..8 {
                        if ui.button(i + 1).clicked() {
                            self.palette = i;
                        }
                    }
                });
                Frame::dark_canvas(ui.style()).show(ui, |ui| {
                    ui.ctx().request_repaint();

                    let desired_size = ui.available_size() * vec2(1.0, 1.0);
                    let (_id, rect) = ui.allocate_space(desired_size);

                    let to_screen = emath::RectTransform::from_to(
                        Rect::from_x_y_ranges(1.0..=256.0, 1.0..=128.0),
                        rect,
                    );

                    let mut shapes = vec![];
                    for table in 0..2 {
                        for i in 0..16 {
                            for j in 0..16 {
                                let index = j * 256 + i * 16;
                                for row in 0..8 {
                                    let address = table * 0x1000 + index + row;
                                    let mut lsb = cart.fetch(address);
                                    let mut msb = cart.fetch(address + 0x08);

                                    for col in 0..8 {
                                        let pixel_color = (lsb & 0x01) + (msb & 0x01);
                                        lsb >>= 1;
                                        msb >>= 1;
                                        let color_rgb =
                                            ppu.get_color_from_palette(self.palette, pixel_color);
                                        let min = Pos2 {
                                            x: (((i * 8) + (7 - col)) + (table * 128)) as f32,
                                            y: ((j * 8) + row) as f32,
                                        };

                                        let max = Pos2 {
                                            x: (((i * 8) + (7 - col)) + (table * 128) + 1) as f32,
                                            y: (((j * 8) + row) + 1) as f32,
                                        };
                                        //println!("Min: {:?} Max: {:?}", min, max);
                                        shapes.push(epaint::Shape::Rect {
                                            rect: egui::Rect {
                                                min: to_screen * min,
                                                max: to_screen * max,
                                            },
                                            corner_radius: 0.0,
                                            fill: egui::Color32::from_rgb(
                                                color_rgb.0,
                                                color_rgb.1,
                                                color_rgb.2,
                                            ),
                                            stroke: egui::Stroke::none(),
                                        });
                                    }
                                }
                            }
                        }
                    }
                    ui.painter().extend(shapes);
                });
            });
    }

    fn render_instruction(&mut self, cpu: &mut MOS6502, ctx: &egui::CtxRef) {
        egui::Window::new("Current Instruction")
            .fixed_size((300.0, 150.0))
            .show(ctx, |ui| {
                let instruction = cpu.instruction();
                ui.label(format!(
                    "Operation: {:?} | Code: {:#2X}",
                    instruction.operation(),
                    instruction.op_code()
                ));
                ui.label(format!("Description: {}", instruction.description()));
                ui.separator();
                ui.label(format!("Addressing Mode: {:?}", instruction.mode()));
                if instruction.address().is_some() {
                    ui.label(format!("Address: {:#06X}", instruction.address().unwrap()));
                    ui.label(format!("Value at Address: {:#02X}", cpu.fetch_w_address()));
                } else {
                    ui.label("Address: N/A");
                }
                ui.separator();
            });
    }

    fn render_ram(&mut self, ram: &[u8], ctx: &egui::CtxRef, name: &str, offset: Option<u16>) {
        const TABLE_WIDTH: usize = 16;
        let grid_key = format!("grid: {}", name);
        egui::Window::new(name)
            .open(self.get_menu_option("option_wram"))
            .fixed_size((530.0, 570.0))
            .show(ctx, |ui| {
                ui.horizontal(|ui| {
                    ui.label(format!("{:04X}:", 0x0000));
                    for b in 0..TABLE_WIDTH {
                        ui.label(format!("{:02X}", b));
                    }
                });
                egui::ScrollArea::auto_sized().show(ui, |ui| {
                    egui::Grid::new(&grid_key).striped(true).show(ui, |ui| {
                        let length = ram.len();
                        let line_count = length / TABLE_WIDTH;
                        for x in 0..line_count {
                            ui.label(format!(
                                "{:04X}:",
                                (x * TABLE_WIDTH) + offset.unwrap_or(0) as usize
                            ));
                            for b in 0..TABLE_WIDTH {
                                ui.label(format!("{:02X}", &ram[b + (x * TABLE_WIDTH)]));
                            }
                            ui.end_row();
                        }
                    });
                })
            });
    }
    fn generate_stack_view(depth: usize, high: usize, stack_ram: &[u8], ui: &mut egui::Ui) {
        if depth == high - 1 {
            return;
        }
        ui.label(format!(
            "Depth {}: {:02X}",
            depth,
            stack_ram[(high - 1) - depth]
        ));
        Self::generate_stack_view(depth + 1, high, stack_ram, ui)
    }

    fn render_stack(stack_values: &[u8], current_sp: u8, ctx: &egui::CtxRef) {
        egui::Window::new("Stack Depth")
            .fixed_size((300.0, 400.0))
            .show(ctx, |ui| {
                egui::ScrollArea::auto_sized().show(ui, |ui| {
                    ui.label(format!("SP: {:#02X}", current_sp));
                    ui.separator();
                    Self::generate_stack_view(0x00, 0x100, stack_values, ui);
                });
            });
    }

    fn render_device_status(&mut self, ctx: &egui::CtxRef, nes: &NES) {
        egui::Window::new("Device Connections")
            .open(self.get_menu_option("option_devices"))
            .fixed_size((200.0, 50.0))
            .show(ctx, |ui| {
                if nes.ppu.borrow().poll_nmi() {
                    ui.colored_label(Color32::GREEN, "PPU NMI");
                } else {
                    ui.label("PPU NMI");
                }
            });
    }

    fn render_palette(&mut self, ctx: &egui::CtxRef) {
        egui::Window::new("NES Palette")
            .fixed_size((256.0, 64.0))
            .open(self.get_menu_option("option_palette"))
            .show(ctx, |ui| {
                Frame::dark_canvas(ui.style()).show(ui, |ui| {
                    ui.ctx().request_repaint();

                    let desired_size = ui.available_size_before_wrap_finite() * vec2(1.0, 1.0);
                    let (_id, rect) = ui.allocate_space(desired_size);

                    let to_screen = emath::RectTransform::from_to(
                        Rect::from_x_y_ranges(0.0..=16.0, 0.0..=4.0),
                        rect,
                    );

                    let mut shapes = vec![];
                    for (i, palette) in DEFAULT_PALETTE.iter().enumerate() {
                        let min = if i == 0 {
                            Pos2::ZERO
                        } else {
                            Pos2 {
                                x: ((i - 1) % 16) as f32,
                                y: ((i - 1) / 16) as f32, //i as f32 * 32.0,
                            }
                        };

                        let max = Pos2 {
                            x: ((i % 16) + 1) as f32,
                            y: ((i / 16) + 1) as f32, //i as f32 * 32.0,
                        };
                        shapes.push(epaint::Shape::Rect {
                            rect: egui::Rect {
                                min: to_screen * min,
                                max: to_screen * max,
                            },
                            corner_radius: 0.0,
                            fill: egui::Color32::from_rgb(palette.0, palette.1, palette.2),
                            stroke: egui::Stroke::none(),
                        });
                    }
                    ui.painter().extend(shapes);
                });
            });
    }
    fn get_menu_option(&mut self, key: &str) -> &mut bool {
        let _ = *self.menu_options.entry(String::from(key)).or_insert(false);
        self.menu_options.get_mut(key).unwrap()
    }

    /// Create the UI using egui.
    fn ui(&mut self, ctx: &egui::CtxRef, emu: &mut Emu) {
        egui::TopBottomPanel::top("menubar_container").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                let mut update_menu_options = |key: String| {
                    *self.menu_options.entry(key).or_insert(false) = true;
                };
                egui::menu::menu(ui, "File", |ui| {
                    if ui.button("Open").clicked() {
                        //self.file_open = true;
                        update_menu_options(String::from("file_open"));
                    }
                    if ui.button("About...").clicked() {
                        update_menu_options(String::from("window_open"));
                    }
                });
                egui::menu::menu(ui, "Options", |ui| {
                    if ui.button("Palette").clicked() {
                        update_menu_options(String::from("option_palette"));
                    }
                    if ui.button("Device Connections").clicked() {
                        update_menu_options(String::from("option_devices"));
                    }
                    if ui.button("CPU Status").clicked() {
                        update_menu_options(String::from("option_status"));
                    }
                    if ui.button("Working Ram").clicked() {
                        update_menu_options(String::from("option_wram"));
                    }
                });
                egui::menu::menu(ui, "Info", |ui| {
                    if ui.button("About...").clicked() {
                        update_menu_options(String::from("info_about"));
                    }
                });
            });
        });

        egui::Window::new("Hello, NES!")
            .open(self.get_menu_option("info_about"))
            .show(ctx, |ui| {
                ui.label("This example demonstrates using egui with pixels.");
                ui.label("Made with 💖 in San Francisco!");

                ui.separator();

                ui.horizontal(|ui| {
                    ui.spacing_mut().item_spacing.x /= 2.0;
                    ui.label("Learn more about egui at");
                    ui.hyperlink("https://docs.rs/egui");
                });
            });
        egui::Window::new("Menu").show(ctx, |ui| {
            if ui.button("Start").clicked() {
                emu.pause = false;
            }
            ui.horizontal(|ui| {
                if ui.button("Step").clicked() {
                    emu.step();
                }
                ui.add(Slider::new(&mut emu.step_count, 0x00..=0xFF));
            });
            if ui.button("Pause").clicked() {
                emu.pause = !emu.pause;
            }
        });

        self.render_palette(ctx);
        self.render_status(ctx, &emu.nes.cpu);
        self.render_instruction(&mut emu.nes.cpu, ctx);
        self.render_instruction_trace(&mut emu.nes.cpu, ctx);
        self.render_pattern_table(emu, ctx);
        self.render_device_status(ctx, &emu.nes);
        self.render_ram(emu.nes.cpu.dump_wram().as_slice(), ctx, "Working Ram", None);
        let bus = emu.nes.bus.borrow();
        let stack = (0x100..0x200).map(|i| bus.fetch(i)).collect::<Vec<u8>>();
        Self::render_stack(&stack, emu.nes.cpu.sp(), ctx);
        //Self::render_ram(&m.prg_rom, &ui, "prg_rom", None);
    }

    /// Render egui.
    pub(crate) fn render(
        &mut self,
        encoder: &mut wgpu::CommandEncoder,
        render_target: &wgpu::TextureView,
        context: &PixelsContext,
    ) {
        // Upload all resources to the GPU.
        self.rpass.update_texture(
            &context.device,
            &context.queue,
            &self.platform.context().texture(),
        );
        self.rpass
            .update_user_textures(&context.device, &context.queue);
        self.rpass.update_buffers(
            &context.device,
            &context.queue,
            &self.paint_jobs,
            &self.screen_descriptor,
        );

        // Record all render passes.
        self.rpass.execute(
            encoder,
            render_target,
            &self.paint_jobs,
            &self.screen_descriptor,
            None,
        );
    }
}
