use std::fs::File;

pub fn load_file_from(path: &str) -> std::io::Result<File> {
    let rom_file = File::open(path)?;
    Ok(rom_file)
}
