#![forbid(unsafe_code)]

use std::cell::RefCell;
use std::collections::BTreeMap;
use std::rc::Rc;

use crate::gui::Gui;
use log::error;
use native_dialog::FileDialog;
use pixels::{Error, Pixels, SurfaceTexture};
use tempest_core::mapper::cartridge::Cart;
use tempest_core::mapper::mapper_000_nrom::NROM;
use tempest_core::mapper::Mapper;
use tempest_core::NES;
use winit::dpi::LogicalSize;
use winit::event::{Event, VirtualKeyCode};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::WindowBuilder;
use winit_input_helper::WinitInputHelper;
mod file_io;

mod gui;

const WIDTH: u32 = 1600;
const HEIGHT: u32 = 900;

pub struct Emu {
    pub nes: NES,
    pub step_count: i32,
    pub pause: bool,
    pub trace: BTreeMap<u16, String>,
    pub cart: Rc<RefCell<dyn Mapper>>,
}

impl Emu {
    fn new(cart: Rc<RefCell<dyn Mapper>>) -> Emu {
        Emu {
            cart: cart.clone(),
            nes: NES::new(cart),
            step_count: 1,
            pause: true,
            trace: BTreeMap::new(),
        }
    }
    fn step(&mut self) {
        for _i in 0..self.step_count {
            self.nes.step();
        }
    }

    fn advance_frame(&mut self) {
        self.nes.frame();
    }
    fn draw(&self, frame: &mut [u8]) {
        let ppu = self.nes.ppu.borrow(); //.get_frame_buffer();
        for (i, pixel) in frame.chunks_exact_mut(4).enumerate() {
            let buffer = ppu.get_frame_buffer();
            let x = i % buffer.width() as usize;
            let y = i / buffer.width() as usize;

            let a = buffer.get_pixel(x, y);
            //let palette_color = DEFAULT_PALETTE[palette_x + (palette_y * 0x10)];
            //let rgba = [palette_color.0, palette_color.1, palette_color.2, 0xff];
            let rgba = [a.0, a.1, a.2, 0xFF];

            pixel.copy_from_slice(&rgba);
        }
    }
}

fn main() -> Result<(), Error> {
    let path = FileDialog::new()
        .add_filter("Nes ROM", &["nes", "bin"])
        .show_open_single_file()
        .unwrap()
        .unwrap();

    let mut file = file_io::load_file_from(path.to_str().unwrap()).expect("File not found");
    env_logger::init();
    let event_loop = EventLoop::new();
    let mut input = WinitInputHelper::new();
    let window = {
        let size = LogicalSize::new(WIDTH as f64, HEIGHT as f64);
        WindowBuilder::new()
            .with_title("Tempest Emulator")
            .with_inner_size(size)
            .with_min_inner_size(size)
            .build(&event_loop)
            .unwrap()
    };

    let (mut pixels, mut gui) = {
        let window_size = window.inner_size();
        let scale_factor = window.scale_factor();
        let surface_texture = SurfaceTexture::new(window_size.width, window_size.height, &window);
        let pixels = Pixels::new(256, 240, surface_texture)?;
        let gui = Gui::new(window_size.width, window_size.height, scale_factor, &pixels);

        (pixels, gui)
    };

    let c = Cart::load(&mut file).unwrap();
    let n = Rc::new(RefCell::new(NROM::load_mapper_from_cart(c)));
    let mut emu = Emu::new(n);
    //emu.trace = emu.cpu.disassembler(0..0xFFFF);
    event_loop.run(move |event, _, control_flow| {
        // Update egui inputs
        gui.handle_event(&event);

        // Draw the current frame
        if let Event::RedrawRequested(_) = event {
            //Render emulator frame
            emu.draw(pixels.get_frame());
            if !emu.pause {
                emu.advance_frame();
            }

            // Prepare egui
            gui.prepare(&mut emu);

            // Render everything together
            let render_result = pixels.render_with(|encoder, render_target, context| {
                // Render the world texture
                context.scaling_renderer.render(encoder, render_target);

                // Render egui
                gui.render(encoder, render_target, context);
            });

            // Basic error handling
            if render_result
                .map_err(|e| error!("pixels.render() failed: {}", e))
                .is_err()
            {
                *control_flow = ControlFlow::Exit;
                return;
            }
        }

        // Handle input events
        if input.update(&event) {
            // Close events
            if input.key_pressed(VirtualKeyCode::Escape) || input.quit() {
                *control_flow = ControlFlow::Exit;
                return;
            }

            // Update the scale factor
            if let Some(scale_factor) = input.scale_factor() {
                gui.scale_factor(scale_factor);
            }

            // Resize the window
            if let Some(size) = input.window_resized() {
                pixels.resize_surface(size.width, size.height);
                gui.resize(size.width, size.height);
            }

            // Update internal state and request a redraw
            window.request_redraw();
        }
    });
}
