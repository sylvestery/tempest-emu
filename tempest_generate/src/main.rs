extern crate tera;
#[macro_use]
extern crate lazy_static;
extern crate serde_json;

use std::fs::File;

use serde_json::value::Value;
use std::error::Error;
use tera::{Context, Tera};

lazy_static! {
    pub static ref TEMPLATES: Tera = {
        let mut tera = match Tera::new("templates/**/*.tera") {
            Ok(t) => t,
            Err(e) => {
                println!("Parsing error(s): {}", e);
                ::std::process::exit(1);
            }
        };
        tera.autoescape_on(vec![]);
        tera
    };
}

fn main() {
    let mut context = Context::new();
    Tera::one_off("hello", &Context::new(), true).unwrap();
    let mut output = File::create("../tempest_core/src/mos6502/disassem.rs").unwrap();
    let json_file = File::open("./templates/cpu/MOS6502Instructions.json").unwrap();

    let p: Value = serde_json::from_reader(&json_file).expect("Malformed Json");
    context.insert("instructions", &p["instructions"]);
    context.insert("output_format", &p["output_format"]);
    context.insert("default_cycles", &p["default_cycles"]);

    match TEMPLATES.render_to("cpu/generate_disassem.tera", &context, &mut output) {
        Ok(_s) => (),
        Err(e) => {
            println!("Error: {}", e);
            let mut cause = e.source();
            while let Some(e) = cause {
                println!("Reason: {}", e);
                cause = e.source();
            }
        }
    };
}
