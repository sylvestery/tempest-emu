use std::{cell::RefCell, rc::Rc};

use map::Bus;
use mapper::Mapper;
use mos6502::cpu::InterruptType;
use mos6502::cpu::MOS6502;

pub mod joypad;
pub mod map;
pub mod mapper;
pub mod mos6502;
pub mod ppu;
use ppu::ppu::PPU;

trait Clocked {
    fn tick(&mut self);
}

pub struct NES {
    pub ppu: Rc<RefCell<PPU>>,
    pub cpu: MOS6502,
    pub cart: Rc<RefCell<dyn Mapper>>,
    pub bus: Rc<RefCell<Bus>>,
    cycles: usize,
}

impl Clocked for NES {
    fn tick(&mut self) {
        //const MASTER_CLOCK: f64 = 21.4772MHZ;
        //let ppu = self.ppu.borrow_mut().
        if self.cycles % 12 == 0 {
            self.cpu.tick();
        }
        if self.cycles % 4 == 0 {
            self.ppu.borrow_mut().tick();
        }

        let io = self.bus.borrow().check_for_nmi_and_irq();
        if io.0 {
            self.cpu.hardware_interrupt(InterruptType::NMI);
        }
        if io.1 {
            self.cpu.hardware_interrupt(InterruptType::IRQ);
        }
        self.cycles = self.cycles.wrapping_add(1);
    }
}
impl NES {
    pub fn new(cart: Rc<RefCell<dyn Mapper>>) -> Self {
        let ppu = Rc::new(RefCell::new(PPU::new_with_cart(cart.clone())));
        let bus = Rc::new(RefCell::new(Bus::new_with_mapper(
            cart.clone(),
            ppu.clone(),
        )));
        let cpu = MOS6502::new_with_bus(bus.clone());

        Self {
            ppu: ppu.clone(),
            bus,
            cpu,
            cart,
            cycles: 0,
        }
    }
    pub fn frame(&mut self) {
        for _j in 0..(256 * 240) {
            for _i in 0..4 {
                self.tick();
            }
        }
    }
    pub fn step(&mut self) {
        for _j in 0..12 {
            self.tick()
        }
    }
}
