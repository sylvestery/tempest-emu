#[allow(dead_code)]
use crate::mapper::cartridge::MirroringMode;

pub struct NameTable {
    name_table: Vec<Vec<u8>>,
    mirror: MirroringMode,
}

const NES_RENDER_WIDTH: usize = 256;
const NES_RENDER_HEIGHT: usize = 240;
impl NameTable {
    pub fn new(mirror: MirroringMode) -> Self {
        const PIXEL_COUNT: usize = NES_RENDER_HEIGHT * NES_RENDER_WIDTH;

        let vec = match mirror {
            MirroringMode::FourScreen => vec![
                vec![0; PIXEL_COUNT],
                vec![0; PIXEL_COUNT],
                vec![0; PIXEL_COUNT],
                vec![0; PIXEL_COUNT],
            ],
            _ => vec![vec![0; PIXEL_COUNT], vec![0; PIXEL_COUNT]],
        };

        Self {
            name_table: vec,
            mirror,
        }
    }
    pub fn read(&self, addr: u16) -> u8 {
        let (block, new_addr) = self.get_mirror_address(addr);

        self.name_table[block as usize][new_addr as usize]
    }
    pub fn write(&mut self, addr: u16, value: u8) {
        let (block, addr) = self.get_mirror_address(addr);
        self.name_table[block as usize][addr as usize] = value;
    }
    pub fn get_mirror_address(&self, addr: u16) -> (u16, u16) {
        let addr = addr - 0x2000;
        let address_block = addr / 0x400;

        match (self.mirror, address_block) {
            (MirroringMode::Horizontal, 0) => (0 as u16, addr),
            (MirroringMode::Horizontal, 1) => (0 as u16, addr - 0x400),
            (MirroringMode::Horizontal, 2) => (1 as u16, addr - 0x800),
            (MirroringMode::Horizontal, 3) => (1 as u16, addr - 0xC00),
            (MirroringMode::Vertical, 0) => (0 as u16, addr),
            (MirroringMode::Vertical, 1) => (1 as u16, addr - 0x400),
            (MirroringMode::Vertical, 2) => (0 as u16, addr - 0x800),
            (MirroringMode::Vertical, 3) => (1 as u16, addr - 0xC00),
            (MirroringMode::Diagonal, 0) => (0 as u16, addr),
            (MirroringMode::Diagonal, 1) => (1 as u16, addr - 0x400),
            (MirroringMode::Diagonal, 2) => (1 as u16, addr - 0x800),
            (MirroringMode::Diagonal, 3) => (0 as u16, addr - 0xC00),
            _ => (address_block, addr),
        }
    }
}
mod tests {

    #[test]
    fn test_vertical_mirror() {
        let mut name_table = super::NameTable::new(super::MirroringMode::Vertical);
        //Test A
        let addr = 0x2801;
        let expected_addr = 0x2001;
        let data = 0x52;
        _check(&mut name_table, addr, expected_addr, data);

        //Test B
        let addr = 0x2C20;
        let expected_addr = 0x2420;
        let data = 0x53;
        _check(&mut name_table, addr, expected_addr, data)
    }
    fn _check(name_table: &mut super::NameTable, addr: u16, expected_addr: u16, data: u8) {
        name_table.write(addr, data);
        assert_eq!(name_table.read(expected_addr), data);
        assert_eq!(name_table.read(addr), data);
    }
    #[test]
    fn test_horizontal_mirror() {
        let mut name_table = super::NameTable::new(super::MirroringMode::Horizontal);
        //Test A
        let addr = 0x2401;
        let expected_addr = 0x2001;
        let data = 0x52;
        _check(&mut name_table, addr, expected_addr, data);
        //Test B
        let addr = 0x2801;
        let expected_addr = 0x2401;
        let data = 0x52;
        _check(&mut name_table, addr, expected_addr, data);
    }
    #[test]
    fn test_diagonal_mirror() {
        let mut name_table = super::NameTable::new(super::MirroringMode::Diagonal);
        //Test A
        let addr = 0x2401;
        let expected_addr = 0x2801;
        let data = 0x52;
        _check(&mut name_table, addr, expected_addr, data);
        //Test B
        let addr = 0x2002;
        let expected_addr = 0x2C02;
        let data = 0x52;
        _check(&mut name_table, addr, expected_addr, data);
    }

    #[test]
    fn test_four_screen_mirror() {
        let mut name_table = super::NameTable::new(super::MirroringMode::FourScreen);
        let a_addr = 0x2001;
        let a_data = 0xAA;
        let b_addr = 0x2401;
        let b_data = 0xBB;
        let c_addr = 0x2801;
        let c_data = 0xCC;
        let d_addr = 0x2C01;
        let d_data = 0xDD;
        name_table.write(a_addr, a_data);
        name_table.write(b_addr, b_data);
        name_table.write(c_addr, c_data);
        name_table.write(d_addr, d_data);
        assert_eq!(name_table.read(a_addr), a_data);
        assert_eq!(name_table.read(b_addr), b_data);
        assert_eq!(name_table.read(c_addr), c_data);
        assert_eq!(name_table.read(d_addr), d_data);
    }
}
