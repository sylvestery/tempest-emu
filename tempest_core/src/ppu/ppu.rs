use crate::{
    map::{Addressable, BusDevice},
    mapper::{cartridge::MirroringMode, mapper_000_nrom::NROM, Mapper},
    ppu::frame_buffer::FrameBuffer,
    Clocked,
};
use bitflags::bitflags;
use log;
use log::warn;
use std::{cell::RefCell, rc::Rc};

use super::{frame_buffer::Pixel, name_table::NameTable, palette::DEFAULT_PALETTE};
//#[allow(dead_code)]
pub struct PPU {
    mirror: MirroringMode,
    frame_buffer: FrameBuffer,
    palette: [u8; 32],
    name_table: NameTable,
    cart: Rc<RefCell<dyn Mapper>>,
    oam_buffer: [u8; 256],
    oam_address: u8,
    pub current_address: PPUAddress,
    temp_address: PPUAddress,
    bus_buffer: u8,
    ctrl: PPUCtrl,
    mask: PPUMask,
    scroll: PPUScroll,
    status: PPUStatus,
    nmi: bool,
    background_state: BackgroundState,
    scanline: isize,
    cycle: usize,
    odd_frame: bool,
}

#[derive(Debug, Default)]
struct BackgroundState {
    pub tile_id: u8,
    pub tile_attrib: u8,
    pub tile_lsb: u8,
    pub tile_msb: u8,
    pub shifter_pattern_lsb: u16,
    pub shifter_pattern_msb: u16,
    pub shifter_attribute_lsb: u16,
    pub shifter_attribute_msb: u16,
}
impl BusDevice for PPU {
    fn poll_nmi(&self) -> bool {
        self.nmi
    }
    fn clear_nmi(&mut self) {
        self.nmi = false
    }
}
impl Clocked for PPU {
    fn tick(&mut self) {
        if self.scanline == -1 && self.cycle == 1 {
            self.status &= !PPUStatus::VBLANK_BEGINNING;
            self.status &= !PPUStatus::SPRITE_OVERFLOW;
            self.status &= !PPUStatus::SPRITE_ZERO_HIT;
            //TODO implement loading of first 16 bytes.
        }
        //TODO Implement 257-320

        if -1 <= self.scanline && self.scanline < 240 {
            if self.scanline == 0 && self.cycle == 0 && self.odd_frame {
                self.cycle = 1
            }
            if (self.cycle >= 1 && self.cycle <= 256) || (self.cycle >= 321) && (self.cycle <= 336)
            {
                self.update_shift_registers();
                match (self.cycle - 1) % 8 {
                    0 => {
                        self.reload_background_shifters();
                        self.background_state.tile_id =
                            self.read(0x2000 | (self.current_address.value & 0x0FFF));
                    }
                    2 => {
                        let (name_table_x, name_table_y) = self.ctrl.name_table();
                        self.background_state.tile_attrib = self.read(
                            0x23C0
                                | ((name_table_y as u16) << 11)
                                | ((name_table_x as u16) << 10)
                                | (((self.scroll.coarse_y() as u16) >> 2) << 3)
                                | ((self.scroll.coarse_x() as u16) >> 2),
                        );
                        if self.scroll.coarse_y() & 0x02 != 0 {
                            self.background_state.tile_attrib >>= 4;
                        }
                        if self.scroll.coarse_x() & 0x02 != 0 {
                            self.background_state.tile_attrib >>= 2;
                        }
                        self.background_state.tile_attrib &= 0x03;
                    }
                    4 => {
                        self.background_state.tile_lsb = self.read(
                            ((self.ctrl.contains(PPUCtrl::BG_TABLE_ADDR) as u16) << 12)
                                + ((self.background_state.tile_id as u16) << 4)
                                + self.scroll.fine_y() as u16,
                        )
                    }
                    6 => {
                        self.background_state.tile_msb = self.read(
                            ((self.ctrl.contains(PPUCtrl::BG_TABLE_ADDR) as u16) << 12)
                                + ((self.background_state.tile_id as u16) << 4)
                                + 0x08,
                        )
                    }

                    7 => { /*TODO Implement scroll*/ }
                    _ => {}
                };
            }
            if self.cycle == 257 {
                self.reload_background_shifters();
            }
            if self.cycle >= 337 && self.cycle <= 340 {
                if self.cycle == 337 || self.cycle == 339 {
                    self.background_state.tile_id =
                        self.read(0x2000 | self.current_address.value & 0x0FFF);
                }
            }
        }

        //Do background composition
        let mut bg_pixel = 0x00;
        let mut bg_palette = 0x00;
        if self.mask.contains(PPUMask::SHOW_BACKGROUND) {
            let multiplex = 0x8000 >> self.scroll.fine_x();
            let lsb_pixel = (self.background_state.shifter_pattern_lsb & multiplex) != 0;
            let msb_pixel = (self.background_state.shifter_pattern_msb & multiplex) != 0;
            bg_pixel = (msb_pixel as u8) + (lsb_pixel as u8);
            let lsb_palette = (self.background_state.shifter_attribute_lsb & multiplex) != 0;
            let msb_palette = (self.background_state.shifter_pattern_msb & multiplex) != 0;
            bg_palette = (msb_palette as u8) + (lsb_palette as u8);
            //println!("Pixel: {} Palette: {}", bg_pixel, bg_palette)
        }
        //Render background text
        let palette_color = self.get_color_from_palette(bg_palette, bg_pixel);
        if 1 <= self.cycle && self.cycle <= 255 && self.scanline >= 0 && self.scanline < 240 {
            self.frame_buffer.set_pixel(
                self.cycle - 1,
                self.scanline as usize,
                Pixel(palette_color.0, palette_color.1, palette_color.2),
            );
        }
        if self.scanline == 240 {
            //NOP
        }
        if self.scanline == 241 && self.cycle == 1 {
            self.status |= PPUStatus::VBLANK_BEGINNING;
            if self.ctrl.contains(PPUCtrl::GENERATE_NMI_BLANK) {
                self.nmi = true;
            }
        }
        self.cycle += 1;
        if self.cycle >= 341 {
            self.scanline += 1;
            self.cycle = 0;
            if self.scanline >= 261 {
                self.scanline = -1;
                self.odd_frame = !self.odd_frame
            }
        }
    }
}

impl Addressable for PPU {
    fn fetch(&self, address: u16) -> u8 {
        match address & 0x2007 {
            // 0x2000 | 0x2001 | 0x2003 | 0x2005 | 0x2006 => {
            // }
            0x2000 => self.ctrl.bits(),
            0x2001 => self.mask.bits(),
            0x2002 => self.status.bits(),
            0x2003 => self.oam_address,
            //0x2002 => self.read_status(),
            //0x2004 => self.read_oam_data(),
            //0x2007 => self.peak_data(),
            _ => {
                warn!("Address {:4x} should never be read from PPU", address);
                0
            }
        }
    }
    fn fetch_mut(&mut self, address: u16) -> u8 {
        match address & 0x2007 {
            0x2000 | 0x2001 | 0x2003 | 0x2005 | 0x2006 => {
                warn!("Attempted read to write only PPU Address{:4X}", address);
                0
            }
            0x2002 => self.read_status(),
            0x2004 => self.read_oam_data(),
            0x2007 => self.read_data(),
            _ => panic!("Address {:4x} should never be read from PPU", address),
        }
    }
    fn write(&mut self, address: u16, value: u8) {
        match address & 0x2007 {
            0x2000 => self.write_ctrl(value),
            0x2001 => self.write_mask(value),
            0x2002 => warn!("Attempted write to read only Status Register."),
            0x2003 => self.write_oam_addr(value),
            0x2004 => self.write_oam_data(value),
            0x2005 => self.write_scroll_buffer(value),
            0x2006 => self.write_addr_buffer(value),
            0x2007 => self.write_data(value),
            _ => panic!("Address {:4x} should never be written into PPU", address),
        }
    }
}
struct PPUScroll {
    scroll_x: u8,
    scroll_y: u8,
    set_x: bool,
}

impl PPUScroll {
    fn update_scroll(&mut self, value: u8) {
        if self.set_x {
            self.scroll_x = value
        } else {
            self.scroll_y = value
        }
        self.set_x = !self.set_x
    }
    pub fn new() -> Self {
        Self {
            scroll_x: 0,
            scroll_y: 0,
            set_x: true,
        }
    }
    pub fn fine_x(&self) -> u8 {
        self.scroll_x & 0x7
    }
    pub fn fine_y(&self) -> u8 {
        self.scroll_y & 0x7
    }
    pub fn coarse_x(&self) -> u8 {
        self.scroll_x & (0xF8) >> 3
    }
    pub fn coarse_y(&self) -> u8 {
        self.scroll_y & (0xF8) >> 3
    }
    fn reset_latch(&mut self) {
        self.set_x = true
    }

    pub fn set_fine_x(&mut self, value: u8) {
        self.scroll_x = (self.scroll_x & 0xF0) | value & 0x7;
    }
    pub fn set_fine_y(&mut self, value: u8) {
        self.scroll_y = (self.scroll_y & 0xF0) | value & 0x7;
    }
    pub fn set_coarse_x(&mut self, value: u8) {
        self.scroll_x = (value & 0xF0) | (self.scroll_x & 0x7);
    }
    pub fn set_coarse_y(&mut self, value: u8) {
        self.scroll_y = (value & 0xF0) | (self.scroll_y & 0x7);
    }
}
impl Default for PPUScroll {
    fn default() -> Self {
        Self::new()
    }
}

/* $2006 PPUADDR http://wiki.nesdev.com/w/index.php/PPU_registers#PPUADDR */

pub struct PPUAddress {
    pub value: u16,
    write_low: bool,
}

impl Default for PPUAddress {
    fn default() -> Self {
        PPUAddress::new()
    }
}

impl Default for PPU {
    fn default() -> Self {
        PPU::new_no_rom()
    }
}

impl PPUAddress {
    pub fn new() -> Self {
        Self {
            value: 0x0000,
            write_low: false, // We start on the high byte.
        }
    }
    pub fn reset_latch(&mut self) {
        self.write_low = false
    }
    pub fn get_addr(&self) -> u16 {
        self.value
    }
    pub fn set_addr(&mut self, new_addr: u16) {
        self.value = new_addr;
    }

    pub fn update_buffer(&mut self, data: u8) {
        if self.write_low {
            self.value &= 0xFF00;
            self.value |= data as u16;
        } else {
            self.value &= 0x00FF;
            self.value |= (data as u16) << 8;
        }
        self.value &= 0x3FFF;

        self.write_low = !self.write_low;
    }

    pub fn increment(&mut self, value: u16) {
        //TODO Check when the carry bit should be handled.
        let addr = self.value as u16; //
        self.value = addr.wrapping_add(value);
    }
    pub fn reset(&mut self) {
        self.write_low = false;
    }
}
//PPU CTRL
bitflags! {
    struct PPUCtrl: u8 {
        const NAME_TABLE_ADDR_LOW = 1 << 0;
        const NAME_TABLE_ADDR_HIGH = 1 << 1;
        const VRAM_ADDR_INC = 1 << 2;
        const SPRITE_TABLE_ADDR = 1 << 3;
        const BG_TABLE_ADDR = 1 << 4;
        const SPRITE_SIZE = 1 << 5;
        const PPU_MODE_SELECT = 1 << 6;
        const GENERATE_NMI_BLANK = 1 << 7;
    }
}

impl Default for PPUCtrl {
    fn default() -> Self {
        Self::new()
    }
}
impl PPUCtrl {
    pub fn new() -> Self {
        PPUCtrl::empty()
    }
    pub fn name_table(&self) -> (bool, bool) {
        (
            self.contains(Self::NAME_TABLE_ADDR_LOW),
            self.contains(Self::NAME_TABLE_ADDR_HIGH),
        )
    }
}

impl Default for PPUMask {
    fn default() -> Self {
        Self::empty()
    }
}

bitflags! {
    struct PPUStatus: u8 {
        const UNUSED1          = 1 << 0;
        const UNUSED2          = 1 << 1;
        const UNUSED3          = 1 << 2;
        const UNUSED4          = 1 << 3;
        const UNUSED5          = 1 << 4;
        const SPRITE_OVERFLOW  = 1 << 5;
        const SPRITE_ZERO_HIT  = 1 << 6;
        const VBLANK_BEGINNING = 1 << 7;
    }
}

impl Default for PPUStatus {
    fn default() -> Self {
        Self::empty()
    }
}
bitflags! {
    struct PPUMask: u8 {
        const GREY_SCALE = 1 << 0;
        const SHOW_BACKGROUND_LEFT_8 = 1 << 1;
        const SHOW_SPRITES_LEFT_8 = 1 << 2;
        const SHOW_BACKGROUND = 1 << 3;
        const SHOW_SPRITES = 1 << 4;
        const EMPHASIZE_RED = 1 << 5;
        const EMPHASIZE_GREEN = 1 << 6;
        const EMPHASIZE_BLUE= 1 << 7;
    }
}

impl PPU {
    pub fn new_no_rom() -> Self {
        let nrom = Rc::new(RefCell::new(NROM::new()));
        Self::new_with_cart(nrom)
    }

    pub fn new_with_cart(cart: Rc<RefCell<dyn Mapper>>) -> Self {
        let mirror = cart.borrow().mirror();
        Self {
            name_table: NameTable::new(mirror),
            palette: [0; 32],
            mirror,
            frame_buffer: FrameBuffer::new(false),
            cart: cart.clone(),
            oam_buffer: [0; 256],
            oam_address: 0,
            current_address: PPUAddress::default(),
            temp_address: PPUAddress::default(),
            bus_buffer: 0,
            ctrl: PPUCtrl::default(),
            mask: PPUMask::default(),
            scroll: PPUScroll::default(),
            status: PPUStatus::default(),
            nmi: false,
            scanline: 0,
            cycle: 0,
            odd_frame: false,
            background_state: BackgroundState::default(),
        }
    }
    pub fn get_frame_buffer(&self) -> &FrameBuffer {
        &self.frame_buffer
    }

    fn output_and_update_bus_buffer(&mut self, value: u8) -> u8 {
        let buffered_value = self.bus_buffer;
        self.bus_buffer = value;
        buffered_value
    }

    fn increment_based_off_ctrl(&mut self) {
        if self.ctrl.intersects(PPUCtrl::VRAM_ADDR_INC) {
            self.current_address.increment(32);
        } else {
            self.current_address.increment(1);
        }
    }

    pub fn peak_data(&self) -> u8 {
        let address = self.current_address.value as usize;
        match address {
            0..=0x1FFF => {
                let out;
                let cart = self.cart.borrow();
                out = cart.ppu_read_chrom(address as u16);
                out
            }
            0x2000..=0x2FFF => self.name_table.read(address as u16),
            _ => {
                warn!("Location 0x{:X} cannot be read.", address);
                0
            }
        }
    }
    fn write_palette(&mut self, addr: u16, value: u8) {
        let addr = addr & 0x1F;
        if addr == 0x10 {
            self.palette[0x00] = value;
        } else {
            self.palette[addr as usize] = value;
        }
    }
    fn read_palette(&self, addr: u16) -> u8 {
        self.palette[(addr & 0x1F) as usize]
    }
    //This function is a read but it does mutate the state of the PPU due to incrementing the address automatically.
    pub fn read_data(&mut self) -> u8 {
        let address = (self.current_address.value as usize) & 0x3FFF;

        let data = match address {
            0..=0x1FFF => {
                let out;
                {
                    let cart = self.cart.borrow();
                    out = cart.ppu_read_chrom(address as u16);
                }
                self.output_and_update_bus_buffer(out)
            }
            0x2000..=0x2FFF => {
                self.output_and_update_bus_buffer(self.name_table.read(address as u16))
            }
            0x3000..=0x3EFF => {
                self.output_and_update_bus_buffer(self.name_table.read((address - 0x1000) as u16))
            }
            0x3F00..=0x3FFF => {
                self.bus_buffer = self.read_palette((address as u16) - 0x3F00);
                self.bus_buffer
            }
            _ => {
                warn!("Location 0x{:X} cannot be read.", address);
                0
            }
        };
        self.increment_based_off_ctrl();
        data
    }

    //For internal PPU use only
    fn read(&self, addr: u16) -> u8 {
        match addr {
            0..=0x1FFF => {
                let cart = self.cart.borrow();
                cart.ppu_read_chrom(addr as u16)
            }
            0x2000..=0x2FFF => self.name_table.read(addr),
            0x3000..=0x3EFF => self.name_table.read(addr - 0x1000),
            0x3F00..=0x3FFF => self.read_palette((addr as u16) - 0x3F00),
            _ => {
                warn!("Location 0x{:X} cannot be read.", addr);
                0
            }
        }
    }

    pub fn get_color_from_palette(&self, palette: u8, pixel: u8) -> (u8, u8, u8) {
        return DEFAULT_PALETTE[self.read_palette(((palette << 2) + pixel) as u16) as usize];
    }

    pub fn write_data(&mut self, value: u8) {
        let address = self.current_address.value as usize;

        match address {
            0..=0x1FFF => warn!("Attempted write to Character Rom in PPU"),
            0x2000..=0x2FFF => self.name_table.write(address as u16, value),
            0x3000..=0x3EFF => self.name_table.write((address - 0x1000) as u16, value),
            0x3F00..=0x3FFF => self.write_palette((address as u16) - 0x3F00, value),
            _ => warn!("Location 0x{:X} cannot be written.", address),
        };
        self.increment_based_off_ctrl();
    }

    pub fn write_oam_addr(&mut self, addr: u8) {
        self.oam_address = addr;
    }

    pub fn write_oam_data(&mut self, value: u8) {
        self.oam_buffer[self.oam_address as usize] = value;
        self.oam_address = self.oam_address.wrapping_add(1);
    }

    pub fn read_oam_data(&self) -> u8 {
        //TODO Implement more detailed OAM read.
        self.oam_buffer[self.oam_address as usize]
    }

    //Remember to read vertical y 240-255 as -16 through -1
    pub fn write_scroll_buffer(&mut self, value: u8) {
        self.scroll.update_scroll(value);
    }

    pub fn name_table(&self) -> &NameTable {
        &self.name_table
    }

    pub fn write_addr_buffer(&mut self, address_half: u8) {
        self.current_address.update_buffer(address_half)
    }

    pub fn write_ctrl(&mut self, value: u8) {
        self.ctrl.bits = value;
    }

    pub fn write_mask(&mut self, value: u8) {
        self.mask.bits = value;
    }

    pub fn read_status(&mut self) -> u8 {
        //Reset address latch.
        //Certain games read the Noise at the end of the PPU Status Register read as if it valid information.
        let out = (self.status.bits & 0xE0) | (self.bus_buffer & 0x1F);
        self.status = !PPUStatus::VBLANK_BEGINNING;
        self.scroll.reset_latch();
        self.current_address.reset_latch();
        out
    }

    fn reload_background_shifters(&mut self) {
        self.background_state.shifter_pattern_lsb = (self.background_state.shifter_pattern_lsb
            & 0xFF00)
            | self.background_state.tile_lsb as u16;
        self.background_state.shifter_pattern_msb = (self.background_state.shifter_pattern_msb
            & 0xFF00)
            | self.background_state.tile_msb as u16;
        //Palettes is the same for 8 bytes at a time. So we turn a valid boolean into 0xFF and invalid 0x00
        self.background_state.shifter_attribute_lsb = (self.background_state.shifter_attribute_lsb
            & 0xFF00)
            | if self.background_state.tile_attrib & 0x1 == 1 {
                0xFF
            } else {
                0x00
            };
        self.background_state.shifter_attribute_msb = (self.background_state.shifter_attribute_msb
            & 0xFF00)
            | if self.background_state.tile_attrib & 0x2 == 1 {
                0xFF
            } else {
                0x00
            };
    }
    fn update_shift_registers(&mut self) {
        if self.mask.contains(PPUMask::SHOW_BACKGROUND) {
            self.background_state.shifter_pattern_lsb <<= 1;
            self.background_state.shifter_pattern_msb <<= 1;

            self.background_state.shifter_attribute_lsb <<= 1;
            self.background_state.shifter_attribute_msb <<= 1;
        }
        //TODO add sprites
    }
    pub fn set_mirror(&mut self, mirror: MirroringMode) {
        self.mirror = mirror;
    }
}

mod test {
    use super::*;

    #[test]
    fn test_vram_write() {
        let mut ppu = PPU::default();
        _ppu_write(&mut ppu, 0x2400, 0x36);
    }

    fn _ppu_write(ppu: &mut PPU, address: u16, data: u8) {
        ppu.write_addr_buffer(((address & 0xFF00) >> 8) as u8);
        ppu.write_addr_buffer((address & 0x00FF) as u8);
        ppu.write_data(data);
        assert_eq!(ppu.current_address.value, address + 1); //Check for auto increment
        assert_eq!(ppu.name_table().read(address), data);
    }
    #[test]
    fn ppu_address_buffer() {
        let mut ppu = PPU::default();
        ppu.write_addr_buffer(0x04);
        ppu.write_addr_buffer(0x20);
        assert_eq!(ppu.current_address.value, 0x0420);
    }

    #[test]
    fn ppu_vram_read() {
        let mut ppu = PPU::default();
        //Default mirroring mode is horizontal.
        let addr = 0x2421;
        let data = 0x70;
        _ppu_write(&mut ppu, addr, data);

        ppu.write_addr_buffer(0x20);
        ppu.write_addr_buffer(0x21);
        ppu.read_data();
        assert_eq!(0x2022, ppu.current_address.value); //Check that increment worked correctly.
        assert_eq!(0x70, ppu.read_data());
    }

    #[test]
    fn ppu_oam_write() {
        let mut ppu = PPU::default();
        ppu.write_oam_addr(0x25);
        ppu.write_oam_data(0x45);
        ppu.write_oam_addr(0x25);
        assert_eq!(ppu.oam_address, 0x25);
        assert_eq!(0x45, ppu.read_oam_data());
    }
    #[test]
    fn ppu_oam_read() {
        let mut ppu = PPU::default();
        ppu.write_oam_addr(0x25);
        ppu.write_oam_data(0x45);
        assert_eq!(ppu.oam_address, 0x26);
        ppu.write_oam_addr(0x25);
        assert_eq!(ppu.oam_address, 0x25);
        assert_eq!(0x45, ppu.read_oam_data());
    }
}
