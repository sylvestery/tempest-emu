// For now we only support NTSC games

const NTSC_WIDTH: usize = 256;
const NTSC_HEIGHT: usize = 240;
const PAL_WIDTH: usize = 256;
const PAL_HEIGHT: usize = 240;
#[derive(Debug, Clone, Copy)]
pub struct Pixel(pub u8, pub u8, pub u8);
pub struct FrameBuffer {
    data: Vec<Pixel>,
    height: usize,
    width: usize,
}

impl Default for FrameBuffer {
    fn default() -> Self {
        Self::new(false)
    }
}
impl FrameBuffer {
    pub fn new(pal_mode: bool) -> Self {
        let (height, width) = if pal_mode {
            (PAL_WIDTH, PAL_HEIGHT)
        } else {
            (NTSC_WIDTH, NTSC_HEIGHT)
        };
        FrameBuffer {
            data: vec![Pixel(0, 0, 0); height * width],
            height,
            width,
        }
    }

    pub fn set_pixel(&mut self, x: usize, y: usize, rgb_value: Pixel) {
        let idx = (y * self.width) + x;
        self.data[idx] = rgb_value;
    }
    pub fn reset(&mut self) {
        self.data = vec![Pixel(0, 0, 0); self.height * self.width];
    }
    pub fn get_pixel(&self, x: usize, y: usize) -> Pixel {
        self.data[(y * self.width) + x]
    }
    pub fn width(&self) -> usize {
        self.width
    }
    pub fn height(&self) -> usize {
        self.height
    }
}
