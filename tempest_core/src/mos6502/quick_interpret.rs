use crate::map::Addressable;
use log::warn;
pub struct ProgramBuffer {
    pub ram: Vec<u8>,
}

impl Addressable for ProgramBuffer {
    fn fetch_mut(&mut self, address: u16) -> u8 {
        self.ram[address as usize]
    }
    fn write(&mut self, _address: u16, _value: u8) {
        warn!("Writes not accepted")
    }
}
