use self::AddressingMode::*;
use self::Direction::*;
use crate::map::Addressable;
use crate::map::Bus;
use crate::mos6502::disassem;
use crate::mos6502::disassem::Operation;
use crate::mos6502::disassem::Operation::*;
use crate::mos6502::quick_interpret::ProgramBuffer;
use crate::Clocked;
use std::cell::RefCell;
use std::collections::BTreeMap;
use std::ops::Range;
use std::rc::Rc;
pub struct MOS6502 {
    reg_a: u8,
    reg_x: u8,
    reg_y: u8,
    sp: u8,
    pc: u16,
    flags: u8,
    cycles: usize,
    cur_instruct: Instruction,
    bus: Rc<RefCell<dyn Addressable>>,

    interpreter_mode: bool,
    // N V - B D I Z C .. V Bit 5 by LSB is not used.
}

pub enum InterruptType {
    Break,
    NMI,
    IRQ,
}

impl Clocked for MOS6502 {
    fn tick(&mut self) {
        if self.cycles > 0 {
            self.cycles -= 1
        } else {
            self.step();
        }
    }
}

#[derive(Debug)]
pub enum AddressingMode {
    Immediate,
    Absolute,
    ZeroPageAbsolute,
    Implied,
    Accumulator,
    AbsoluteIndexed(Register),
    ZeroPageIndexed(Register),
    Indirect,
    IndexedIndirect,
    IndirectIndexed,
    Relative,
}
// Each one takes a full address hence the 2 byte difference between each.
const NMI_VECTOR: u16 = 0xFFFA;
const RESET_VECTOR: u16 = 0xFFFC;
const IRQ_VECTOR: u16 = 0xFFFE;

trait Addressing {
    fn immediate_address(&mut self) -> u16;
    fn absolute_address(&mut self) -> u16;
    fn absolute_indexed(&mut self, register: Register) -> u16;
    fn zero_page(&mut self) -> u16;
    fn zero_page_indexed(&mut self, register: Register) -> u16;
    fn indirect_address(&mut self) -> u16; // //Only used by the jmp instruction.
    fn indexed_indirect_address(&mut self) -> u16;
    fn indirect_indexed_address(&mut self) -> u16;
    fn relative_address(&mut self) -> u16;
}
enum Direction {
    Left,
    Right,
}

#[derive(Debug)]
pub struct Instruction {
    address: Option<u16>,
    op: Operation,
    op_code: u8,
    mode: AddressingMode,
    cycles: usize,
    description: String,
}

impl Instruction {
    pub fn new(
        op: Operation,
        op_code: u8,
        mode: AddressingMode,
        cycles: usize,
        des: &str,
    ) -> Instruction {
        Instruction {
            address: None,
            op,
            op_code,
            mode,
            cycles,
            description: des.to_owned(),
        }
    }

    pub fn address(&self) -> Option<u16> {
        self.address
    }

    pub fn operation(&self) -> &Operation {
        &self.op
    }

    pub fn op_code(&self) -> u8 {
        self.op_code
    }

    pub fn mode(&self) -> &AddressingMode {
        &self.mode
    }

    pub fn description(&self) -> &str {
        &self.description
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Register {
    A,
    X,
    Y,
}

//TODO Replace with bitflags! macro.
#[derive(Debug, Clone, Copy)]
pub enum Status {
    Carry,
    Zero,
    InterruptDisable,
    Decimal,
    Unused,
    Break,
    Overflow,
    Negative,
}
impl Status {
    fn offset(&self) -> u8 {
        match self {
            Status::Carry => 0,
            Status::Zero => 1,
            Status::InterruptDisable => 2,
            Status::Decimal => 3,
            Status::Break => 4,
            Status::Unused => 5,
            Status::Overflow => 6,
            Status::Negative => 7,
        }
    }
}

impl Addressable for MOS6502 {
    fn fetch_mut(&mut self, address: u16) -> u8 {
        self.bus.borrow_mut().fetch_mut(address)
    }

    fn write(&mut self, address: u16, value: u8) {
        self.bus.borrow_mut().write(address, value)
    }
}

impl Addressing for MOS6502 {
    fn immediate_address(&mut self) -> u16 {
        let addr = self.pc;
        self.pc = self.pc.wrapping_add(1);
        addr as u16
    }

    fn absolute_address(&mut self) -> u16 {
        // Read in the next two bytes to get the address.
        let pcl = self.fetch_mut(self.pc);
        let pch = self.fetch_mut(self.pc + 1);
        self.pc = self.pc.wrapping_add(2);
        MOS6502::construct_pc(pcl, pch)
    }

    fn absolute_indexed(&mut self, register: Register) -> u16 {
        let address = self
            .absolute_address()
            .wrapping_add(self.fetch_register(register) as u16);

        if !self.are_same_page(address) {
            self.increment_cycles();
        }
        address
    }

    fn zero_page(&mut self) -> u16 {
        let pcl = self.fetch_mut(self.pc);
        self.pc = self.pc.wrapping_add(1);
        pcl as u16
    }

    fn zero_page_indexed(&mut self, register: Register) -> u16 {
        let zero_page_value = (self.zero_page() & 0xFF) as u8;
        zero_page_value.wrapping_add(self.fetch_register(register)) as u16
    }
    // //Only used by the jmp instruction.
    fn indirect_address(&mut self) -> u16 {
        let mut pcl = self.fetch_mut(self.pc);
        let mut pch = self.fetch_mut(self.pc + 1);

        // TODO add more documentation for this bug.
        let pc_ptr_low = (pcl as u16) | ((pch as u16) << 8);
        let pc_ptr_high = ((pcl.wrapping_add(1)) as u16) | ((pch as u16) << 8);

        pcl = self.fetch_mut(pc_ptr_low);
        pch = self.fetch_mut(pc_ptr_high);
        self.pc = self.pc.wrapping_add(2);
        MOS6502::construct_pc(pcl, pch)
    }

    fn indexed_indirect_address(&mut self) -> u16 {
        let temp: u16 = self
            .fetch_with_pc()
            .wrapping_add(self.fetch_register(Register::X)) as u16;
        let pcl: u8 = self.fetch_mut(temp);
        let pch: u8 = self.fetch_mut(temp + 1);
        self.pc = self.pc.wrapping_add(1);
        MOS6502::construct_pc(pcl, pch)
    }

    fn indirect_indexed_address(&mut self) -> u16 {
        let base = self.fetch_with_pc() as u16;
        let pcl = self.fetch_mut(base);
        let pch = self.fetch_mut(base + 1);
        self.pc = self.pc.wrapping_add(1);
        MOS6502::construct_pc(pcl, pch).wrapping_add(self.fetch_register(Register::Y) as u16)
    }

    // Used by branching instructions, serves by representing an address with a 8 bit signed offset. Range: [-128, 127]
    fn relative_address(&mut self) -> u16 {
        let offset = self.fetch_mut(self.pc) as i8;

        let ad = self.pc.wrapping_add(1).wrapping_add(offset as u16);
        self.pc = self.pc.wrapping_add(1);

        if !self.are_same_page(ad) {
            self.increment_cycles_by_value(2);
        }

        ad
    }
}

impl Default for MOS6502 {
    fn default() -> Self {
        Self::new()
    }
}

//Implement an alternative for bus
impl MOS6502 {
    pub fn interpret(
        &mut self,
        instruction_buffer: &[u8],
        maximum_instruction: usize,
        reset_pc: bool,
    ) -> Result<(), String> {
        self.interpreter_mode = true;

        let ram: Vec<u8> = if instruction_buffer.len() != 0x7FFF {
            let mut ram_ = vec![0; 0x7FFF];
            for i in 0..instruction_buffer.len() {
                ram_[i] = instruction_buffer[i];
            }
            ram_
        } else {
            instruction_buffer.to_owned()
        };
        let new_bus = Rc::new(RefCell::new(ProgramBuffer { ram }));
        self.bus = new_bus;
        self.pc = if reset_pc { 0 } else { self.pc - 1 };
        let mut count = 0;
        while !self.get_flag(Status::Break) {
            if count >= maximum_instruction {
                return Err(
                    "Maximum instruction execution limit reached, likely due to infinite loop"
                        .to_owned(),
                );
            }
            self.step();
            count += 1;
        } // Does not equal BRK
        self.interpreter_mode = false;
        self.set_flag(Status::Break, false);
        Ok(())
    }

    pub fn sp(&self) -> u8 {
        self.sp
    }

    pub fn pc(&self) -> u16 {
        self.pc
    }

    pub fn set_pc(&mut self, pc_: u16) {
        self.pc = pc_;
    }

    pub fn register(&self, register: Register) -> u8 {
        self.fetch_register(register)
    }

    pub fn instruction(&self) -> &Instruction {
        &self.cur_instruct
    }

    pub fn cycles(&self) -> usize {
        self.cycles
    }

    pub fn status(&self) -> &MOS6502 {
        self
    }

    pub fn status_mut(&mut self) -> &mut MOS6502 {
        self
    }

    pub fn new() -> MOS6502 {
        MOS6502 {
            reg_a: 0,
            reg_x: 0,
            reg_y: 0,
            sp: 0xFD,
            pc: RESET_VECTOR, //Change back to reset when implemented.
            flags: 0x00,
            cycles: 0,
            cur_instruct: Instruction::new(NOP, 0xEA, Implied, 0, "Boot Up"),
            bus: Rc::new(RefCell::new(Bus::default())),
            interpreter_mode: false,
        }
    }
    pub fn new_with_bus(bus: Rc<RefCell<dyn Addressable>>) -> MOS6502 {
        let mut o2 = MOS6502 {
            reg_a: 0,
            reg_x: 0,
            reg_y: 0,
            sp: 0xFD,
            pc: RESET_VECTOR, //Change back to reset when implemented.
            flags: 0x00,
            cycles: 0,
            cur_instruct: Instruction::new(NOP, 0xEA, Implied, 0, "Boot Up"),
            bus: bus.clone(),
            interpreter_mode: false,
        };
        o2.reset();
        o2
    }

    fn construct_pc(pcl: u8, pch: u8) -> u16 {
        (pcl as u16) | ((pch as u16) << 8)
    }

    fn fetch_with_pc(&mut self) -> u8 {
        self.fetch_mut(self.pc)
    }

    fn fetch_register(&self, register: Register) -> u8 {
        match register {
            Register::A => self.reg_a,
            Register::X => self.reg_x,
            Register::Y => self.reg_y,
        }
    }

    pub fn reset(&mut self) {
        const STACK_INITIAL_VALUE: u8 = 0xFD;
        self.reg_a = 0;
        self.reg_x = 0;
        self.reg_y = 0;
        self.flags = 0;
        self.sp = STACK_INITIAL_VALUE;
        let pcl = self.fetch_mut(RESET_VECTOR);
        let pch = self.fetch_mut(RESET_VECTOR + 1);
        self.pc = MOS6502::construct_pc(pcl, pch);
        self.fetch_next_instruction();
    }

    #[allow(dead_code)]
    fn are_same_page(&mut self, address: u16) -> bool {
        self.fetch_address() ^ address == 0
    }

    fn increment_cycles_by_value(&mut self, value: usize) {
        self.cycles += value;
    }

    fn increment_cycles(&mut self) {
        self.increment_cycles_by_value(1);
    }

    pub fn step(&mut self) {
        self.fetch_next_instruction();
        self.execute();
    }

    fn fetch_next_instruction(&mut self) {
        let op_code = self.fetch_mut(self.pc);
        self.cur_instruct = disassem::decode_instruction(op_code);
    }

    pub fn dump_sram(&mut self) -> Vec<u8> {
        let mut out = Vec::new();
        for i in 0x6000..=0x7FFF {
            out.push(self.fetch_mut(i));
        }
        out
    }

    pub fn dump_wram(&mut self) -> Vec<u8> {
        let mut out = Vec::new();
        for i in 0x0000..=0x07FF {
            out.push(self.fetch_mut(i));
        }
        out
    }

    fn execute(&mut self) {
        self.pc += 1; //Automatically increment by one
        let mut accumulator: bool = false;
        self.cur_instruct.address = match self.cur_instruct.mode {
            Implied => None,
            Immediate => Some(self.immediate_address()),
            Absolute => Some(self.absolute_address()),
            AbsoluteIndexed(reg) => Some(self.absolute_indexed(reg)),
            Indirect => Some(self.indirect_address()),
            IndexedIndirect => Some(self.indexed_indirect_address()),
            IndirectIndexed => Some(self.indirect_indexed_address()),
            Relative => Some(self.relative_address()),
            ZeroPageAbsolute => Some(self.zero_page()),
            ZeroPageIndexed(reg) => Some(self.zero_page_indexed(reg)),
            Accumulator => {
                accumulator = true;
                None
            }
        };

        match self.cur_instruct.op {
            ADC => self.adc(),
            SBC => self.sbc(),
            ASL => self.shift(accumulator, Left),
            BRK => self.brk(),
            BCC => self.branch_clear(Status::Carry),
            BCS => self.branch_equal(Status::Carry),
            BEQ => self.branch_equal(Status::Zero),
            BNE => self.branch_clear(Status::Zero),
            BIT => self.bit(),
            BMI => self.branch_equal(Status::Negative),
            BPL => self.branch_clear(Status::Negative),
            BVC => self.branch_clear(Status::Overflow),
            BVS => self.branch_equal(Status::Overflow),
            NOP => self.nop(),
            CLC => self.clr(Status::Carry),
            CLD => self.clr(Status::Decimal),
            CLI => self.clr(Status::InterruptDisable),
            CLV => self.clr(Status::Overflow),
            CMP => self.compare(Register::A),
            CPX => self.compare(Register::X),
            CPY => self.compare(Register::Y),
            AND => self.and(),
            ORA => self.ora(),
            EOR => self.eor(),
            DEC => self.dec(),
            DEX => self.dex(),
            DEY => self.dey(),
            INC => self.inc(),
            INX => self.inx(),
            INY => self.iny(),
            JMP => self.jmp(),
            JSR => self.jsr(),
            LDA => self.load(Register::A),
            LDX => self.load(Register::X),
            LDY => self.load(Register::Y),
            LSR => self.shift(accumulator, Right),
            PHA => self.pha(),
            PHP => self.php(),
            PLA => self.pla(),
            PLP => self.plp(),
            SEC => self.set(Status::Carry),
            SED => self.set(Status::Decimal),
            SEI => self.set(Status::InterruptDisable),
            ROL => self.rotate(accumulator, Left),
            ROR => self.rotate(accumulator, Right),
            RTI => self.rti(),
            RTS => self.rts(),
            STA => self.store(Register::A),
            STX => self.store(Register::X),
            STY => self.store(Register::Y),
            TXS => self.txs(),
            TSX => self.tsx(),
            TAX => self.tax(),
            TAY => self.tay(),
            TXA => self.txa(),
            TYA => self.tya(),
        };

        //how many cycles to tka to execute the instruction.
        self.cycles = self.cur_instruct.cycles;
    }

    pub fn get_flag(&self, flag: Status) -> bool {
        (self.flags & (1 << flag.offset())) != 0
    }

    fn set_flag(&mut self, flag: Status, value: bool) {
        if value {
            self.flags |= 1 << flag.offset();
        } else {
            self.flags &= !(1 << flag.offset());
        }
    }

    fn push(&mut self, value: u8) {
        const STACK_INITIAL: u16 = 0x100;
        self.write(STACK_INITIAL + (self.sp as u16), value);
        self.sp = self.sp.wrapping_sub(1);
    }

    fn push_16(&mut self, value: u16) {
        self.push((value >> 8) as u8);
        self.push((value & 0xFF) as u8);
    }

    fn pull(&mut self) -> u8 {
        self.sp = self.sp.wrapping_add(1);
        const STACK_INITIAL: u16 = 0x100;
        self.fetch_mut(STACK_INITIAL + (self.sp as u16))
    }

    fn pull_16(&mut self) -> u16 {
        let lo = self.pull();
        let hi = self.pull();
        MOS6502::construct_pc(lo, hi)
    }

    fn push_pc(&mut self) {
        self.push_16(self.pc);
    }

    /* Forces an interrupt during execution
    // Pushes program counter and flags to stack.
     Sets break flag and program counter is set to the interrupt vector set at 0xFFFF.
    */
    fn brk(&mut self) {
        //Break useful for debugging.
        //
        //Use the simpler version for the interpreter bad things will happen otherwise
        if self.interpreter_mode {
            self.set_flag(Status::Break, true);
        } else {
            self.push_16(self.pc + 1);
            self.set_flag(Status::Break, true);
            self.push(self.flags); //Push status register.
            self.set_flag(Status::Break, false);

            self.pc = self.fetch_16(IRQ_VECTOR);
        }
    }
    fn fetch_16(&mut self, addr: u16) -> u16 {
        let pcl = self.fetch_mut(addr);
        let pch = self.fetch_mut(addr + 1);
        MOS6502::construct_pc(pcl, pch)
    }

    #[allow(unused_variables)]
    pub fn hardware_interrupt(&mut self, t: InterruptType) {
        self.push_pc();

        self.set_flag(Status::Break, false);
        self.set_flag(Status::Unused, true); //Dont know why but its set to true;
        self.set_flag(Status::InterruptDisable, true);
        self.push(self.flags);

        //match t {
        //    InterruptType::NMI => self.cycles = 8,
        //    InterruptType::Break => (),
        //    InterruptType::IRQ => (),
        //}
        self.pc = self.fetch_16(NMI_VECTOR);
        self.cycles = 8;
    }

    fn branch_clear(&mut self, flag: Status) {
        let condition = self.get_flag(flag);

        if !condition {
            self.pc = self.fetch_address();
            self.increment_cycles();
        }
    }

    fn branch_equal(&mut self, flag: Status) {
        let condition = self.get_flag(flag);

        if condition {
            self.pc = self.fetch_address();
            self.increment_cycles()
        }
    }

    fn rti(&mut self) {
        self.flags = self.pull();
        self.pc = self.pull_16();
    }

    fn rts(&mut self) {
        //The value returned is of course the current instruction so if we returned we would be stuck in an infinite loop.
        self.pc = self.pull_16() + 1;
    }

    fn bit(&mut self) {
        let result = self.reg_a & self.fetch_w_address();
        self.set_flag(Status::Negative, (result & 0x80) != 0);
        self.set_flag(Status::Overflow, (result & 0x40) != 0);
        self.set_zero(result);
    }

    fn set(&mut self, flag: Status) {
        self.set_flag(flag, true);
    }

    fn clr(&mut self, flag: Status) {
        self.set_flag(flag, false);
    }

    fn dec(&mut self) {
        let temp = self.fetch_w_address().wrapping_sub(1);
        self.set_nz(temp);
        self.write_address(temp);
    }

    fn dex(&mut self) {
        self.reg_x = self.reg_x.wrapping_sub(1); //Should prevent the program from crashing if the program underflows.
        self.set_nz(self.reg_x);
    }

    fn dey(&mut self) {
        self.reg_y = self.reg_y.wrapping_sub(1);
        self.set_nz(self.reg_y);
    }

    fn inc(&mut self) {
        let temp = self.fetch_w_address().wrapping_add(1);
        self.set_nz(temp);
        self.write(self.cur_instruct.address.unwrap(), temp);
    }

    fn inx(&mut self) {
        self.reg_x = self.reg_x.wrapping_add(1);
        self.set_nz(self.reg_x);
    }

    fn iny(&mut self) {
        self.reg_y = self.reg_y.wrapping_add(1);
        self.set_nz(self.reg_y);
    }

    //It turns out that adc and sbc are basically the same with all the bits flipped for the operand.
    fn addition(&mut self, value: u8, carry_: bool) {
        let reg_a = self.reg_a as u16;
        let operand = value as u16;
        let carry = carry_ as u16;
        if !self.get_flag(Status::Decimal) {
            let result = reg_a + operand + carry;
            let overflow: bool = (((reg_a ^ operand) & (reg_a ^ result)) & 0x80) != 0;

            self.set_nz(result as u8);
            self.set_flag(Status::Carry, result > 0xFF);
            self.set_flag(Status::Overflow, overflow);

            self.reg_a = (result & 0xFF) as u8;
        } else {
            //TODO implement decimal mode
            unimplemented!();
        }
    }

    fn adc(&mut self) {
        let operand = self.fetch_w_address();
        let carry = self.get_flag(Status::Carry);
        self.addition(operand, carry);
    }

    fn sbc(&mut self) {
        let operand: u8 = self.fetch_w_address();
        let carry: bool = self.get_flag(Status::Carry);
        self.addition(!operand, !carry);
    }

    fn nop(&self) {
        //Do nothing. On a real cpu this implemented with a combination of instructions, here it doesn't matter.
    }

    fn and(&mut self) {
        let operand = self.fetch_w_address();
        let result = operand & self.reg_a;
        self.set_nz(result);
        self.reg_a = result;
    }

    fn ora(&mut self) {
        let operand = self.fetch_w_address();
        let result = self.reg_a | operand;

        self.set_nz(result);
        self.reg_a = result;
    }

    fn eor(&mut self) {
        let operand = self.fetch_w_address();
        let result = self.reg_a ^ operand;

        self.set_nz(result);
        self.reg_a = result;
    }

    fn jmp(&mut self) {
        self.pc = self.fetch_address();
    }

    fn jsr(&mut self) {
        self.push_16(self.pc.wrapping_sub(1));
        self.pc = self.fetch_address();
    }

    fn pha(&mut self) {
        self.push(self.reg_a);
    }

    fn php(&mut self) {
        //For some reason the 6502 when pushing its processor status sends the value with the break flag and reserved flag enabled.
        // self.set_flag(Status::Break, true);
        // FIXME this code is ugly.
        self.push(self.flags | (1 << Status::Break.offset()) | (1 << Status::Unused.offset()));
    }

    fn pla(&mut self) {
        self.reg_a = self.pull();
        self.set_nz(self.reg_a);
    }

    fn plp(&mut self) {
        self.flags = self.pull();
    }

    fn set_negative(&mut self, value: u8) {
        // And with this value 1000 0000
        self.set_flag(Status::Negative, (value & 0x80) != 0);
    }

    fn set_zero(&mut self, value: u8) {
        self.set_flag(Status::Zero, value == 0);
    }
    //Since these two flags often occur at the same place make helper function that executes both.
    fn set_nz(&mut self, value: u8) {
        self.set_zero(value);
        self.set_negative(value);
    }

    //Load a byte from a memory address
    fn load(&mut self, destination_reg: Register) {
        let value = self.fetch_w_address();
        match destination_reg {
            Register::A => self.reg_a = value,
            Register::X => self.reg_x = value,
            Register::Y => self.reg_y = value,
        }
        self.set_nz(value);
    }

    fn store(&mut self, source_reg: Register) {
        let value = match source_reg {
            Register::A => self.reg_a,
            Register::X => self.reg_x,
            Register::Y => self.reg_y,
        };

        self.write_address(value);
    }

    fn compare(&mut self, register: Register) {
        let memory_value = self.fetch_w_address();
        let reg_value = match register {
            Register::A => self.reg_a,
            Register::X => self.reg_x,
            Register::Y => self.reg_y,
        };

        let result = reg_value.wrapping_sub(memory_value);
        //Alternatively carry could be set with result >= 0 but that wasn't as  cool
        self.set_flag(Status::Carry, reg_value >= memory_value);
        self.set_nz(result);
    }

    fn shift(&mut self, accumulator: bool, direction: Direction) {
        let temp = if accumulator {
            self.reg_a
        } else {
            self.fetch_w_address()
        };

        let result = match direction {
            Left => {
                self.set_flag(Status::Carry, (temp & 0x80) != 0);
                temp << 1
            }
            Right => {
                self.set_flag(Status::Carry, (temp & 0x01) != 0);
                temp >> 1
            }
        };

        self.set_nz(result);

        if accumulator {
            self.reg_a = result;
        } else {
            self.write(self.cur_instruct.address.unwrap(), result);
        }
    }

    fn rotate(&mut self, accumulator: bool, direction: Direction) {
        let temp = if accumulator {
            self.reg_a
        } else {
            self.fetch_w_address()
        };
        let old_carry = self.get_flag(Status::Carry);
        let result = match direction {
            Left => {
                // 0100 0000
                self.set_flag(Status::Carry, (temp & 0x80) != 0);
                (temp << 1) | (old_carry as u8)
            }
            Right => {
                // 0000 0001
                self.set_flag(Status::Carry, (temp & 0x01) != 0);
                ((old_carry as u8) << 7) | (temp >> 1)
            }
        };
        if accumulator {
            self.reg_a = result;
        } else {
            self.write_address(result);
        }
    }

    fn tax(&mut self) {
        self.reg_x = self.reg_a;
        self.set_nz(self.reg_x);
    }
    fn tay(&mut self) {
        self.reg_y = self.reg_a;
        self.set_nz(self.reg_y)
    }

    fn txa(&mut self) {
        self.reg_a = self.reg_x;
        self.set_nz(self.reg_a);
    }

    fn tya(&mut self) {
        self.reg_a = self.reg_y;
        self.set_nz(self.reg_a);
    }

    fn tsx(&mut self) {
        self.reg_x = self.sp;
        self.set_nz(self.reg_x);
    }

    fn txs(&mut self) {
        self.sp = self.reg_x;
    }

    fn fetch_address(&self) -> u16 {
        match self.cur_instruct.address {
            Some(addr) => addr,
            None => 0x00,
        }
    }

    pub fn fetch_w_address(&mut self) -> u8 {
        self.fetch_mut(self.fetch_address())
    }

    fn write_address(&mut self, value: u8) {
        self.write(self.cur_instruct.address.unwrap(), value);
    }
    pub fn disassembler(&mut self, range: Range<u16>) -> BTreeMap<u16, String> {
        let mut result = BTreeMap::new();

        let mut addr = range.start;
        while addr <= range.end {
            let addr_op = addr;
            let mut line = format!("${:04X}: ", addr);
            let bus = self.bus.borrow();
            let op_code = bus.fetch(addr as u16);
            let instruction = disassem::decode_instruction(op_code);
            line.push_str(&format!("{:?} ", instruction.operation()));
            addr += 1;
            match instruction.mode() {
                Implied => {}
                Immediate => {
                    let lo = bus.fetch(addr);
                    addr += 1;
                    line.push_str(&format!("#${:02X}", lo));
                }
                ZeroPageAbsolute => {
                    let lo = bus.fetch(addr);
                    addr += 1;
                    line.push_str(&format!("${:02X}", lo));
                }
                ZeroPageIndexed(reg) => {
                    let lo = bus.fetch(addr);
                    addr += 1;
                    let letter = if reg == &Register::X { "X" } else { "Y" };
                    line.push_str(&format!("${:02X}, {}", lo, letter));
                }
                IndexedIndirect => {
                    let lo = bus.fetch(addr);
                    addr += 1;
                    line.push_str(&format!("(${:02X}, X)", lo));
                }
                IndirectIndexed => {
                    let lo = bus.fetch(addr);
                    addr += 1;
                    line.push_str(&format!("(${:02X}), Y", lo));
                }
                Absolute => {
                    let lo = bus.fetch(addr);
                    addr += 1;
                    let hi = bus.fetch(addr);
                    addr += 1;
                    line.push_str(&format!("${:04X}", MOS6502::construct_pc(lo, hi)));
                }
                AbsoluteIndexed(reg) => {
                    let lo = bus.fetch(addr);
                    addr += 1;
                    let hi = bus.fetch(addr);
                    addr += 1;
                    let letter = if reg == &Register::X { "X" } else { "Y" };
                    line.push_str(&format!(
                        "${:04X}, {}",
                        MOS6502::construct_pc(lo, hi),
                        letter
                    ));
                }
                Indirect => {
                    let lo = bus.fetch(addr);
                    addr += 1;
                    let hi = bus.fetch(addr);
                    addr += 1;
                    line.push_str(&format!("(${:04X})", MOS6502::construct_pc(lo, hi)));
                }
                Relative => {
                    let v: i8 = bus.fetch(addr) as i8;
                    addr += 1;
                    line.push_str(&format!(
                        "${:02X} (${:04X})",
                        v,
                        addr.wrapping_add(v as u16)
                    ));
                }
                Accumulator => {}
            }
            //println!("{}", line);
            result.insert(addr_op, line);
        }
        return result;
    }
}

mod test {

    #[allow(unused_imports)]
    use super::*;
    #[test]
    fn test_increment() {
        let mut cpu = MOS6502::default();
        cpu.interpret(&mut [0xE8, 0xC8, 0x00], 1000, true).unwrap(); // I will make this the basic version.
        assert_eq!(cpu.register(Register::A), 0x00);
        assert_eq!(cpu.register(Register::X), 0x01);
        assert_eq!(cpu.register(Register::Y), 0x01);
        assert_eq!(cpu.pc(), 0x03);
    }

    #[test]
    fn test_flag_set() {
        let mut cpu = MOS6502::default();
        cpu.interpret(&mut [0x38, 0xf8, 0x78, 0x00], 1000, true)
            .unwrap(); // I will make this the basic version.
        assert!(cpu.get_flag(Status::Carry));
        assert!(cpu.get_flag(Status::Decimal));
        assert!(cpu.get_flag(Status::InterruptDisable));
        assert_eq!(cpu.pc(), 0x04);
    }

    #[test]
    fn test_flag_clear() {
        let mut cpu = MOS6502::default();
        cpu.interpret(&mut [0x38, 0xf8, 0x78, 0x18, 0xD8, 0x58, 0x00], 1000, true)
            .unwrap();
        assert!(!cpu.get_flag(Status::Carry));
        assert!(!cpu.get_flag(Status::Decimal));
        assert!(!cpu.get_flag(Status::InterruptDisable));
        assert_eq!(cpu.pc(), 0x07);
    }

    #[test]
    fn test_load_immediate() {
        let mut cpu = MOS6502::default();
        cpu.interpret(&mut [0xA9, 0x49, 0x00], 4, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0x49);
        assert_eq!(cpu.pc(), 0x03);
        cpu.interpret(&mut [0xA2, 0x49, 0x00], 4, true).unwrap();
        assert_eq!(cpu.register(Register::X), 0x49);
        assert_eq!(cpu.pc(), 0x03);
        cpu.interpret(&mut [0xA0, 0x49, 0x00], 4, true).unwrap();
        assert_eq!(cpu.register(Register::Y), 0x49);
        assert_eq!(cpu.pc(), 0x03);
    }

    #[test]
    fn test_transfer() {
        let mut cpu = MOS6502::default();
        cpu.interpret(&mut [0xA9, 0x20, 0xA2, 0x42, 0xA0, 0x35, 0x00], 4, true)
            .unwrap();
        assert_eq!(cpu.register(Register::A), 0x20);
        assert_eq!(cpu.register(Register::X), 0x42);
        assert_eq!(cpu.register(Register::Y), 0x35);
        assert_eq!(cpu.pc(), 0x07);
        cpu.interpret(&mut [0xAA, 0xA8, 0x00], 10, true).unwrap();
        assert_eq!(cpu.register(Register::A), cpu.register(Register::X));
        assert_eq!(cpu.register(Register::A), cpu.register(Register::Y));
        assert_eq!(cpu.pc(), 0x03);
    }

    #[test]
    fn test_load_accumulator_all_modes() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0x00F1] = 0x32; // Zero Page Absolute Address
        ram[0x00F2] = 0x33; // Zero Page Indirect Address
        ram[0x1FFF] = 0x96; // Absolute Reference
        ram[0x2052] = 0x97; // Absolute Reference
        ram[0x2053] = 0x37; // Absolute Reference
        ram[0x0031] = 0x32; // Indirect First
        ram[0x0032] = 0x20; // Indirect Second
        ram[0x2032] = 0x48;
        ram[0x2042] = 0x36;

        //Test Immediate
        ram[0] = 0xA9;
        ram[1] = 0x20;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0x20);
        assert_eq!(cpu.pc(), 0x03);

        //Test Zero Page
        ram[0] = 0xA5;
        ram[1] = 0xF1;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0x32);
        assert_eq!(cpu.pc(), 0x03);

        //Test absolute
        ram[0] = 0xAD;
        ram[1] = 0xFF;
        ram[2] = 0x1F;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0x96);
        assert_eq!(cpu.pc(), 0x04);
        //Test IndexedIndirect
        ram[0] = 0xA2;
        ram[1] = 0x11;
        ram[2] = 0xA1;
        ram[3] = 0x20;
        cpu.interpret(&mut ram, 500, true).unwrap();
        assert_eq!(cpu.register(Register::X), 0x11);
        assert_eq!(cpu.register(Register::A), 0x48);
        //Test IndirectIndex
        ram[0] = 0xA0;
        ram[1] = 0x10;
        ram[2] = 0xB1;
        ram[3] = 0x31;
        cpu.interpret(&mut ram, 500, true).unwrap();
        assert_eq!(cpu.register(Register::Y), 0x10);
        assert_eq!(cpu.register(Register::A), 0x36);

        //Test Zero Page Indexed
        ram[0] = 0xA2;
        ram[1] = 0x01;
        ram[2] = 0xB5;
        ram[3] = 0xF1;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::X), 0x01);
        assert_eq!(cpu.register(Register::A), 0x33);
        assert_eq!(cpu.pc(), 0x05);

        //Test Absolute Indexed X
        ram[0] = 0xA2;
        ram[1] = 0x53;
        ram[2] = 0xBD;
        ram[3] = 0xFF;
        ram[4] = 0x1F;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert!(cpu.get_flag(Status::Negative));
        assert!(!cpu.get_flag(Status::Zero));
        assert_eq!(cpu.register(Register::A), 0x97);
        //Test Absolute Indexed Y
        ram[0] = 0xA0;
        ram[1] = 0x54;
        ram[2] = 0xB9;
        ram[3] = 0xFF;
        ram[4] = 0x1F;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert!(!cpu.get_flag(Status::Negative));
        assert!(!cpu.get_flag(Status::Zero));
        assert_eq!(cpu.register(Register::A), 0x37);
    }

    #[test]
    fn test_jmp_modes() {
        let mut cpu = MOS6502::default();
        cpu.interpret(&mut [0x4C, 0x44, 0x2F], 20, true).unwrap();
        assert_eq!(cpu.pc() - 1, 0x2F44); //We have to subtract by one due to the automatic increment at the end of the the interpret.
        let mut ram = vec![0x00; 0x7FFF];

        //This section is specifically to check the jump indirect bug, that exist in early MOS6502s and the 2A03 found in the NES.
        ram[0x3000] = 0x40;
        ram[0x30FF] = 0x80;
        ram[0x3100] = 0x50;
        ram[0] = 0x6C;
        ram[1] = 0xFF;
        ram[2] = 0x30;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.pc() - 1, 0x4080); //We have to subtract by one due to the automatic increment at the end of the the interpret.

        ram[0x3205] = 0x80;
        ram[0x3206] = 0x50;
        ram[0] = 0x6C;
        ram[1] = 0x05;
        ram[2] = 0x32;
        cpu.interpret(&mut ram, 50, true).unwrap();
        assert_eq!(cpu.pc() - 1, 0x5080); //We have to subtract by one due to the automatic increment at the end of the the interpret.
    }

    #[test]
    fn test_branching() {
        let mut cpu = MOS6502::default();
        cpu.interpret(&mut [0x38, 0xB0, 0x40], 20, true).unwrap();
        assert_eq!(cpu.pc() - 1, 0x43);
    }

    #[test]
    fn test_bit() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0xB0] = 0b11_00_0000;

        ram[0] = 0xA9;
        ram[1] = 0xC0;
        ram[2] = 0x24;
        ram[3] = 0xB0;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert!(cpu.get_flag(Status::Negative));
        assert!(cpu.get_flag(Status::Overflow));
        assert!(!cpu.get_flag(Status::Zero));
    }
    #[test]
    fn test_or() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0] = 0xA9;
        ram[1] = 0x55;
        ram[2] = 0x09;
        ram[3] = 0x99;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0xDD);
    }

    #[test]
    fn test_and() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0] = 0xA9;
        ram[1] = 0x55;
        ram[2] = 0x29;
        ram[3] = 0x99;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0x11);
    }

    #[test]
    fn test_eor() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0] = 0xA9;
        ram[1] = 0x55;
        ram[2] = 0x49;
        ram[3] = 0x99;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0xCC);
    }
    #[test]
    fn test_adc() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0] = 0xA9;
        ram[1] = 0x55;
        ram[2] = 0x69;
        ram[3] = 0x11;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0x66);
    }

    #[test]
    fn test_sbc() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0] = 0xA9;
        ram[1] = 0x55;
        ram[2] = 0xE9;
        ram[3] = 0x11;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0x44);
    }
    #[test]
    fn test_asl() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0] = 0xA9;
        ram[1] = 0x01;
        ram[2] = 0x0A;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), (1 << 1));
    }
    #[test]
    fn test_lsr() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0] = 0xA9;
        ram[1] = 0x80;
        ram[2] = 0x4A;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), (0x80 >> 1));
    }
    #[test]
    fn test_rol() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0] = 0xA9;
        ram[1] = 0x81;
        ram[2] = 0x2A;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0x02);
        assert!(cpu.get_flag(Status::Carry));
        cpu.interpret(&[0x2A], 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0x05);
        assert!(!cpu.get_flag(Status::Carry));
    }
    #[test]
    fn test_ror() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0] = 0xA9;
        ram[1] = 0x81;
        ram[2] = 0x6A;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0x40);
        assert!(cpu.get_flag(Status::Carry));
        cpu.interpret(&[0x6A], 20, true).unwrap();
        assert_eq!(cpu.register(Register::A), 0xA0);
        assert!(!cpu.get_flag(Status::Carry));
    }

    #[test]
    fn test_compare() {
        let mut cpu = MOS6502::default();
        let mut ram = vec![0x00; 0x7FFF];
        ram[0] = 0xA9;
        ram[1] = 0x81;
        ram[2] = 0xC9;
        ram[3] = 0x81;
        cpu.interpret(&mut ram, 20, true).unwrap();
        assert!(cpu.get_flag(Status::Carry));
        assert!(!cpu.get_flag(Status::Negative));
        assert!(cpu.get_flag(Status::Zero));
        cpu.interpret(&[0xA9, 0x40, 0xC9, 0x55], 20, true).unwrap();
        assert!(!cpu.get_flag(Status::Carry));
        assert!(cpu.get_flag(Status::Negative));
        assert!(!cpu.get_flag(Status::Zero));
        cpu.interpret(&[0xA9, 0x57, 0xC9, 0x55], 20, true).unwrap();
        assert!(cpu.get_flag(Status::Carry));
        assert!(!cpu.get_flag(Status::Negative));
        assert!(!cpu.get_flag(Status::Zero));
    }
}
