use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

use crate::mapper::mapper_000_nrom::NROM;
use crate::mapper::Mapper;
use crate::ppu::ppu::PPU;

use log::warn;
pub trait Addressable {
    fn fetch(&self, _address: u16) -> u8 {
        0
    }
    fn fetch_mut(&mut self, _address: u16) -> u8 {
        0
    }
    fn write(&mut self, address: u16, value: u8);
}
pub trait BusDevice {
    fn poll_nmi(&self) -> bool {
        false
    }
    fn poll_irq(&self) -> bool {
        false
    }
    fn clear_nmi(&mut self) {}
    fn clear_irq(&mut self) {}
}
pub struct Bus {
    ram: Box<[u8; 0x800]>,
    mapped_cart: Rc<RefCell<dyn Mapper>>, //Basically each cart will have a mapper to the original data
    ppu: Rc<RefCell<PPU>>,
    devices: HashMap<String, Rc<RefCell<dyn BusDevice>>>,
}
impl Addressable for Bus {
    fn fetch(&self, address: u16) -> u8 {
        match address {
            0x0000..=0x1FFF => self.ram[(address & 0x7FF) as usize],
            0x2000..=0x3FFF => self.ppu.borrow().fetch(address & 0x2007),
            0x4016..=0x4017 => 0,
            0x4020..=0xFFFF => self.mapped_cart.borrow().fetch(address),
            _ => {
                warn!("Unimplemented access at {:4x}", address);
                0
            }
        }
    }
    fn fetch_mut(&mut self, address_16: u16) -> u8 {
        let address = address_16 as usize;
        let ret: u8 = match address {
            0x0000..=0x1FFF => self.ram[address & 0x7FF],
            0x2000..=0x3FFF => self.ppu.borrow_mut().fetch_mut(address_16 & 0x2007),
            0x4016..=0x4017 => 0,
            0x4020..=0xFFFF => self.mapped_cart.borrow().fetch(address as u16),
            _ => {
                warn!("Unimplemented access at {:4x}", address);
                0
            }
        };
        ret
    }
    fn write(&mut self, address: u16, value: u8) {
        match address {
            0x0000..=0x1FFF => self.ram[(address as usize) & 0x7FF] = value,
            //PPU Addresses
            0x2000..=0x3FFF => self.ppu.borrow_mut().write(address & 0x2007, value),
            0x4014 => {
                let mut oam_transfer_buffer: [u8; 256] = [0; 256];
                for i in 0..=255u16 {
                    oam_transfer_buffer[i as usize] = self.fetch_mut(((value as u16) << 8) | i)
                    //TODO INCREMENT THE CYCLES
                }
            }
            0x4020..=0xFFFF => self.mapped_cart.borrow_mut().write(address as u16, value),
            _ => {
                warn!("Unimplemented access at {:4x}", address);
            }
        }
    }
}

impl Default for Bus {
    fn default() -> Self {
        let nrom = Rc::new(RefCell::new(NROM::new()));
        let ppu = Rc::new(RefCell::new(PPU::new_with_cart(nrom.clone())));
        Self::new_with_mapper(nrom, ppu)
    }
}
impl Bus {
    pub fn new_with_mapper(cart: Rc<RefCell<dyn Mapper>>, ppu: Rc<RefCell<PPU>>) -> Self {
        let mut bus = Self {
            ram: Box::new([0; 2048]),
            mapped_cart: cart.clone(), //Basically each cart will have a mapper to the original data
            ppu: ppu.clone(),
            devices: HashMap::new(),
        };
        bus.add_device("PPU", ppu.clone());
        bus
    }

    pub fn add_device(&mut self, name: &str, device: Rc<RefCell<dyn BusDevice>>) {
        self.devices.insert(String::from(name), device);
    }

    pub fn get_device_status(&self, name: &str) -> bool {
        self.devices[name].borrow().poll_nmi()
    }

    pub fn check_for_nmi_and_irq(&self) -> (bool, bool) {
        let mut nmi = false;
        let mut irq = false;
        for device in self.devices.values() {
            let mut dev = device.borrow_mut();
            if dev.poll_nmi() {
                nmi = true;
                dev.clear_nmi();
            }
            irq |= dev.poll_irq();
        }
        (nmi, irq)
    }
}

mod test {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn test_bus_ram_mirror() {
        let mut bus = Bus::default();
        bus.write(0x1973, 0x40);
        assert_eq!(bus.fetch_mut(0x0173), 0x40);
        assert_eq!(bus.fetch_mut(0x0973), 0x40);
        assert_eq!(bus.fetch_mut(0x1173), 0x40);
        assert_eq!(bus.fetch_mut(0x1973), 0x40);
    }
}
