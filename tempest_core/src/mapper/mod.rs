use crate::map::Addressable;

pub mod cartridge;
pub mod mapper_000_nrom;

pub trait Mapper: Addressable {
    fn mirror(&self) -> cartridge::MirroringMode;
    fn ppu_read_chrom(&self, addr: u16) -> u8;
}
