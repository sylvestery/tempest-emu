use byteorder::{BigEndian, ReadBytesExt};
use std::io::prelude::{Read, Seek};
use std::io::SeekFrom;
#[derive(Debug, Clone, Copy)]
pub enum MirroringMode {
    Horizontal,
    Vertical,
    FourScreen,
    Diagonal,
}
//Temporary Object that will be converted into a mapper later on.
#[derive(Debug, Clone)]
pub struct Cart {
    // All this will need to get done before the mapper is implemented.
    pub mirror: MirroringMode,
    pub mapper: u8,
    pub prg_rom: Vec<u8>,
    pub ch_rom: Vec<u8>,
    pub has_save: bool,
}

fn check_bit(value: u8, bit: u8) -> bool {
    if bit > 7 {
        return false;
    }
    (value & (1 << bit)) >> bit == 1
}

const CH_ROM_BANK_SIZE: usize = 8;
const PRG_ROM_BANK_SIZE: usize = 16;
impl Cart {
    pub fn load<R: Read + Seek>(rom: &mut R) -> Result<Cart, String> {
        let magic = rom.read_u32::<BigEndian>().unwrap();
        const INES_MAGIC: u32 = 0x4E45531A;
        if magic != INES_MAGIC {
            return Err("Not an INES File".to_owned());
        }
        let prg_rom_banks = rom.read_u8().unwrap();
        let ch_rom_banks = rom.read_u8().unwrap();
        let flag6 = rom.read_u8().unwrap();
        let flag7 = rom.read_u8().unwrap();
        let _flag8 = rom.read_u8().unwrap();
        let _flag9 = rom.read_u8().unwrap();
        let _flag10 = rom.read_u8().unwrap();
        rom.seek(SeekFrom::Current(5)).unwrap();
        let mapper = (flag7 & 0xF0) | (flag6 & 0xF0) >> 4;

        let four_screen = flag6 & 0x04 != 0;
        let vertical_mirror = flag6 & 0x01 != 0;

        let mirror = if four_screen {
            MirroringMode::FourScreen
        } else if vertical_mirror {
            MirroringMode::Vertical
        } else {
            MirroringMode::Horizontal
        };

        let mut prg_rom: Vec<u8> = vec![0; (prg_rom_banks as usize) * PRG_ROM_BANK_SIZE * 1024];
        let mut ch_rom: Vec<u8> = vec![0; (ch_rom_banks as usize) * CH_ROM_BANK_SIZE * 1024];

        if check_bit(flag6, 2) {
            rom.seek(SeekFrom::Start(16 + 512)).unwrap(); //Skip training info and go to the data.
        }

        rom.read_exact(prg_rom.as_mut_slice()).unwrap();
        rom.read_exact(ch_rom.as_mut_slice()).unwrap();

        Ok(Cart {
            mirror,
            mapper,
            prg_rom,
            ch_rom,
            has_save: true,
        })
    }
}
