use super::cartridge::MirroringMode;
use super::Addressable;
use super::Mapper;
use crate::mapper::cartridge::Cart;
use log::{error, info};

pub struct NROM {
    prg_ram: Box<[u8; 0x2000]>, //This is the same as SRAM
    prg_rom: Box<[u8]>,         //Will have one of two sizes.
    chr_rom: Box<[u8]>,
    mirror: MirroringMode,
}
impl Addressable for NROM {
    fn fetch(&self, address: u16) -> u8 {
        match address {
            0x0000..=0x1FFF => self.chr_rom[address as usize],
            0x6000..=0x7FFF => self.prg_ram[(address - 0x6000) as usize],
            0x8000..=0xFFFF => {
                let mut addr = address - 0x8000;
                if self.prg_rom.len() == 0x4000 && addr >= 0x4000 {
                    addr = addr % 0x4000;
                }
                self.prg_rom[addr as usize]
            }
            _ => {
                info!("Unimplemented read at address 0x{:4X}", address);
                0
            }
        }
    }
    fn write(&mut self, address: u16, value: u8) {
        match address {
            0x6000..=0x7FFF => self.prg_ram[(address - 0x6000) as usize] = value,
            _ => error!("WRITES UNSUPPORTED"),
        }
    }
}

impl Mapper for NROM {
    fn mirror(&self) -> MirroringMode {
        self.mirror
    }
    fn ppu_read_chrom(&self, addr: u16) -> u8 {
        self.fetch(addr)
    }
}

impl NROM {
    pub fn new() -> Self {
        NROM {
            prg_ram: Box::new([0; 0x2000]),
            prg_rom: Box::new([0; 0x2000]),
            chr_rom: Box::new([0; 0x2000]),
            mirror: MirroringMode::Horizontal,
        }
    }

    pub fn load_mapper_from_cart(cart: Cart) -> Self {
        Self {
            prg_ram: Box::new([0; 0x2000]),
            prg_rom: cart.prg_rom.into(),
            chr_rom: cart.ch_rom.into(),
            mirror: cart.mirror,
        }
    }
}

impl Default for NROM {
    fn default() -> Self {
        NROM::new()
    }
}
mod test {
    #[allow(unused_imports)]
    use super::*;
    #[test]
    fn test_chr_rom_init() {
        let test_values = NROM {
            prg_ram: Box::new([0; 0x2000]),
            prg_rom: Box::new([0; 0x2000]),
            chr_rom: Box::new([0; 0x2000]),
            mirror: MirroringMode::Horizontal,
        };
        for i in 0..=0x2000 {
            assert_eq!(test_values.fetch(i), 0);
        }
    }
}
