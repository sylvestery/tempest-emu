// NES JoyPad input
// Multi-tap unsupported

use bitflags::bitflags;

bitflags! {
pub struct JoyPadStrobe: u8 {
    const A      = 1 << 0;
    const B      = 1 << 1;
    const SELECT = 1 << 2;
    const START  = 1 << 3;
    const UP     = 1 << 4;
    const DOWN   = 1 << 5;
    const LEFT   = 1 << 6;
    const RIGHT  = 1 << 7;
}
}

pub struct JoyPad {}
